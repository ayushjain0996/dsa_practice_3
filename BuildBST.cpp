#include<iostream>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
node * BuildBST(int arr[], int n){
    if(n<=0){
        return NULL;
    }
    if(n%2){
        node * root = new node(arr[n/2]);
        root->left = BuildBST(arr, n/2);
        root->right = BuildBST(&arr[n/2+1], n/2);
        return root;
    }
    else{
        node * root = new node(arr[n/2-1]);
        root->left = BuildBST(arr, n/2-1);
        root->right = BuildBST(&arr[n/2], n/2);
        return root;
    }
}
void PrintBST(node * root){
    if(root==NULL){
        return;
    }
    cout<< root->data <<" ";
    PrintBST(root->left);
    PrintBST(root->right);
    return;
}
int main(){
    int T;
    cin>>T;
    while(T--){
        int n;
        cin>>n;
        int arr[1000];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        node * root = BuildBST(arr, n);
        PrintBST(root);
        cout<<endl;
    }
    return 0;
}