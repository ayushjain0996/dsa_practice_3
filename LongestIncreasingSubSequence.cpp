#include<iostream>
#include<algorithm>

using namespace std;

int lis(int arr[], int n){
    int * DP = new int[n];
    DP[0] = 1;
    for(int i=1; i<n; i++){
        DP[i] = 1;
        for(int j=0; j<i; j++){
            if(arr[j]<arr[i] && DP[i]<DP[j] + 1){
                DP[i] = DP[j] + 1;
            }
        }
    }
    int result = *max_element(DP, DP + n);
    
    delete[] DP;
    return result;
}

int main(){
    int n;
    cin>>n;
    int * arr = new int[n];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    cout<<lis(arr, n)<<endl;
    delete[] arr;

    return 0;
}