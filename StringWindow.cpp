#include<iostream>
#include<string>
#include<limits>

using namespace std;

const int noOfChars = 256; 

string findSubstring(string str, string pat){
    int len1 = str.length();
    int len2 = pat.length();
    if(len2 > len1){
        return "";
    }

    int hashPat[noOfChars] = {0};
    int hashStr[noOfChars] = {0};

    for(int i=0; i<len2; i++){
        hashPat[pat[i]]++;
    }

    int count = 0, start = 0;
    int startIndex = -1, minLen = 10001;

    for(int j=0; j<len1; j++){
        hashStr[str[j]]++;

        if(hashPat[str[j]]!=0 && hashStr[str[j]] <= hashPat[str[j]]){
            count++;
        }

        if(count==len2){
            while(hashPat[str[start]]==0 || hashPat[str[start]]<hashStr[str[start]]){
                if(hashPat[str[start]] < hashStr[str[start]]){
                    hashStr[str[start]]--;
                }
                start++;
            }

            int lenWindow = j - start + 1;
            if(minLen > lenWindow){
                minLen = lenWindow;
                startIndex = start;
            }
        }
    }
    if(startIndex==-1){
        return "";
    }
    else{
        return str.substr(startIndex, minLen);
    }
}

int main(){
    string str, pat;
    getline(cin, str);
    getline(cin, pat);
    cout<<findSubstring(str, pat);
    return 0;
}