#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

//arr[y][x]  y->m, x->n

int greatestSum(int arr[][100], int m, int n){
    int r = 1;
    int sum = 0;
    for(int x=n-1; x>=0; x--){
        int count1 = 0;
        for(int y=0; y<m; y++){
            if(arr[y][x]==1){
                count1++;
            }
        }
        if(count1<=m/2){
            count1 = m - count1;
        }
        sum = count1*r + sum;
        r = r * 2;
    }
    return sum;
}

int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */ 
    int t;
    cin>>t;
    while(t--){
        int m,n;
        cin>>m>>n;
        int arr[100][100];
        for(int y=0; y<m; y++){
            for(int x=0; x<n; x++){
                cin>>arr[y][x];
            }
        }
        cout<<greatestSum(arr, m, n)<<endl;
    }
    return 0;
}
