#include<iostream>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};

node * addNode(node * root, int val){
    if(root==NULL){
        root = new node(val);
        return root;
    }
    if(root->data > val){
        root->left = addNode(root->left, val);
    }
    else{
        root->right = addNode(root->right, val);
    }
    return root;
}
node * buildBST(int arr[], int n){
    node * root = NULL;
    for(int i=0; i<n; i++){
        root = addNode(root, arr[i]);
    }
    return root;
}

void greaterSum(node * root, int * sum){
    if(root==NULL){
        return;
    }
    greaterSum(root->right, sum);
    *sum = *sum + root->data;
    root->data = *sum;
    greaterSum(root->left, sum);
    return;
}
void modifySumBST(node * root){
    int sum = 0;
    greaterSum(root, &sum);
    return;
}

void printPreOrder(node * root){
    if(root==NULL){
        return;
    }
    cout<< root->data <<" ";
    printPreOrder(root->left);
    printPreOrder(root->right);
    return;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int arr[1000];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        node * root = buildBST(arr, n);
        printPreOrder(root);
        cout<<endl;
        modifySumBST(root);
        printPreOrder(root);
        cout<<endl;
    }
    return 0;
}