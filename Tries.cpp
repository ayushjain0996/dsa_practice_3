#include<iostream>
#include<vector>

using namespace std;

const int ALPHABET_SIZE = 26;
const int CASE = 'a';

struct node{
    node * parent;
    node * children[ALPHABET_SIZE];
    int occurrences;
    node(){
        parent = NULL;
        occurrences = 0;
        for(int i=0; i<ALPHABET_SIZE; i++){
            children[i] = NULL;
        }
    }
};

void InsertNode(node * trieTree, char* word){
    node * currentNode = trieTree;

    while(*word != '\0'){
        if(currentNode->children[*word - CASE] == NULL){
            currentNode->children[*word - CASE] = new node();
            currentNode->children[*word - CASE]->parent = currentNode;
        }

        currentNode = currentNode->children[*word - CASE];
        ++word;
    }

    ++(currentNode->occurrences);
}
node * Search(node * trieTree, char* word){
    while(*word != '\0'){
        if(trieTree->children[*word - CASE] != NULL){
            trieTree = trieTree->children[*word - CASE];
            ++word;
        }
        else{
            return NULL;
        }
    }
    return (trieTree->occurrences!=0) ? trieTree : NULL;
}

void DeleteNode(node * trieTree, char* word){
    node * currNode = Search(trieTree, word);

    if(currNode != NULL){
        --(currNode->occurrences);
        node * parent = NULL;
        bool isLeaf = true;

        for(int i=0; i<ALPHABET_SIZE; i++){
            if(currNode->children[i] != NULL){
                isLeaf = false;
                break;
            }
        }

        while(currNode->parent != NULL && isLeaf == true && currNode->occurrences==0){
            for(int i=0; i<ALPHABET_SIZE; i++){
                if(parent->children[i] == currNode){
                    parent->children[i] = NULL;
                    delete currNode;
                    currNode = parent;
                }
                else if(parent->children[i] != NULL){
                    isLeaf = false;
                    break;
                }

            }
        }
    }
    return;
}

void PreOrderPrint(node * trieTree, vector<char> word){
    if(trieTree->occurrences > 0){
        for(auto i = word.begin(); i != word.end(); ++i){
            cout<< *i;
        }
        cout<<" "<< trieTree->occurrences <<endl;
    }

    for(int i=0; i<ALPHABET_SIZE; i++){
        if(trieTree->children[i] != NULL){
            word.push_back(CASE + i);
            PreOrderPrint(trieTree->children[i], word);
            word.pop_back();
        }
    }
    return;
}

int main(){
    vector<char> word;
    node * trieTree = new node();
    char str[1000];
    int n;
    cin>>n;
    for(int i=0; i<n; i++){
        cin>>str;
        InsertNode(trieTree, str);
    }

    cout<<endl;
    PreOrderPrint(trieTree, word);
    return 0;
}