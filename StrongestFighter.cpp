#include<iostream>
#include<queue>
#include<vector>

using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * CreateLL(int n){
    if(n==0){
        return NULL;
    }
    n--;
    int x;
    cin>>x;
    node * head = new node(x);
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
int findMax(queue<int> Q){
    int max = 0;
    while(!Q.empty()){
        int x = Q.front();
        if(x>max){
            max = x;
        }
        Q.pop();
    }
    return max;
}
int main(){
    int n;
    cin>>n;
    node * head = CreateLL(n);
    int q;
    cin>>q;
    node * p1 = head;
    queue<int> line;
    vector<int> strongest;
    int len = 0;
    while(q--){
        line.push(p1->data);
        p1 = p1->next;
    }
    while(p1){
        int max = findMax(line);
        strongest.push_back(max);
        len++;
        line.pop();
        line.push(p1->data);
        p1 = p1->next;
    }
    int max = findMax(line);
    strongest.push_back(max);
    len++;
    for (int i = 0; i < len; i++){
        cout << strongest[i] << " "; 
    }
    return 0;
}