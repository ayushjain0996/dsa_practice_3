#include<iostream>
#include<algorithm>

using namespace std;

bool mycomp(int a, int b){
    int r1 = 1, r2 = 1;
    while(r1<=a){
        r1*=10;
    }
    while (r2<=b)
    {
        r2*=10;
    }
    long long int num1 = a*r2*1LL + b;
    long long int num2 = b*r1*1LL + a;
    return (num1>num2);
}

bool compare(int a, int b){
    return (a>b);
}

int main(){
    int n;
    cin>>n;
    int * arr = new int[n];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    sort(arr, arr+n, mycomp);
    for(int i=0; i<n; i++){
        cout<<arr[i]<<" ";
    }   
    delete[] arr;
    return 0;
}