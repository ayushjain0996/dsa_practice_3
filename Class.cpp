#include<iostream>

using namespace std;

class apple{
    int data;

    public:
    apple(int data){
        cout<<"This is a constructor"<<endl;
        this->data = data;
    }
    ~apple(){
        cout<<"This is a destructor."<<endl;
    }
    void colour(){
        cout<<"This is a red apple."<<endl;
    }
};

int main(){
    apple a(1);
    a.colour();
    a.~apple();
    a.colour();
    return 0;
}