#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * createLL(int n){
    if(n==0){
        return NULL;
    }
    int x;
    cin>>x;
    node * head = new node(x);
    n--;
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
void printLL(node * head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    cout<<endl;
    return;
}
void OddEven(node *& head){
    node * EvenHead = NULL;
    node * EvenTail;
    node * OddHead;
    node * OddTail;
    node * element = head;
    if((head->data)%2==0){
        OddHead = head;
        OddTail = head;
        element = element->next;
        while(element){
            if((element->data)%2==0){
                EvenHead = element;
                EvenTail = element;
                element = element->next;
                break;
            }
            else{
                OddTail->next = element;
                OddTail = OddTail->next;
                element = element->next;
            }
        }
        while(element){
            if((element->data)%2==1){
                OddTail->next = element;
                OddTail = OddTail->next;
                element = element->next;
            }
            else{
                EvenTail->next = element;
                EvenTail = EvenTail->next;
                element = element->next;
            }
        }
        OddTail->next=EvenHead;
        EvenTail->next = NULL;
        head = OddHead;
        return;
    }
    else{
        EvenHead = head;
        EvenTail = head;
        element = element->next;
        while(element){
            if((element->data)%2==1){
                OddHead = element;
                OddTail = element;
                element = element->next;
                break;
            }
            else{
                EvenTail->next = element;
                EvenTail = EvenTail->next;
                element = element->next;
            }
        }
        while(element){
            if((element->data)%2==1){
                OddTail->next = element;
                OddTail = OddTail->next;
                element = element->next;
            }
            else{
                EvenTail->next = element;
                EvenTail = EvenTail->next;
                element = element->next;
            }
        }
        if(OddTail!=NULL){
            OddTail->next=EvenHead;
        }
        else{
            OddHead = EvenHead;
        }  
        EvenTail->next = NULL;
        head = OddTail;
        return;
    }
}
int main(){
    int n;
    cin>>n;
    node * head = createLL(n);
    OddEven(head);
    printLL(head);
    return 0;
}