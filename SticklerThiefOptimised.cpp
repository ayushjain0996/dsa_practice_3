//  https://practice.geeksforgeeks.org/problems/stickler-theif/0

#include<iostream>
#include<algorithm>

using namespace std;

int maxSum(int arr[], int n){
    int excl = 0;
    int excl_new = 0;
    int incl = 0;
    for(int i=0; i<n; i++){
        excl_new = max(incl, excl);
        incl = excl + arr[i];
        excl = excl_new;
    }
    return max(excl, incl);
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int * arr = new int[n];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        cout<< maxSum(arr, n) <<endl;
        delete[] arr;
    }
    return 0;
}