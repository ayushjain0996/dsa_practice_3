#include<iostream>
#include<queue>

using namespace std;
void reverse(queue<int> &Q){
    //Base Condition
    if(Q.empty()){
        return;
    }
    //Recursive Call
    int x = Q.front();
    Q.pop();
    reverse(Q);
    Q.push(x);
    return;
}

int main(){
    int n;
    cin>>n;
    queue<int> Q;
    for(int i=0; i<n; i++){
        int x;
        cin>>x;
        Q.push(x);
    }
    reverse(Q);
    while(!Q.empty()){
        cout<< Q.front() <<" ";
        Q.pop();
    }
    cout<<"END";
    return 0;
}