#include<iostream>
#include<stack>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
//Input -> 5 0 3 0 2 -1 -1 0 1 -1 -1 0 4 0 6 -1 -1 0 7 -1 -1
node * createTree(){
    node * root;
    int val;
    cin>>val;
    root = new node(val);
    int x;
    cin>>x;
    if(x!=-1){
        root->left = createTree();
    }
    cin>>x;
    if(x!=-1){
        root->right = createTree();
    }
    return root;
}

void inOrder(node * root){
    if(root==NULL){
        return;
    }
    inOrder(root->left);
    cout<<root->data<<" ";
    inOrder(root->right);
    return;
}

void inOrderIterative(node * root){
    node * curr = root;
    stack<node *> s;
    while(curr!=NULL || !s.empty()){
        while(curr!=NULL){
            s.push(curr);
            curr = curr->left;
        }
        curr = s.top();
        s.pop();
        cout<< curr->data <<" ";
        curr = curr->right;
    }
    return;
}

int main(){
    node * root = createTree();
    inOrder(root);
    cout<<endl;
    inOrderIterative(root);
    return 0;
}