#include<iostream>
#include<algorithm>

using namespace std;

int SCS(char str1[], char str2[]){
    int m = strlen(str1);
    int n = strlen(str2);
    int ** arr = new int*[m+1];
    for(int i=0; i<=m; i++){
        arr[i] = new int[n+1];
    }
    for(int i=0; i<=m; i++){
        for(int j=0; j<=n; j++){
            if(i==0 || j==0){
                arr[i][j] = 0;
            }
            else if(str1[i-1] == str2[j-1]){
                arr[i][j] = arr[i-1][j-1] + 1;
            }
            else{
                arr[i][j] = max(arr[i-1][j], arr[i][j-1]);
            }
        }
    }
    int lcs = arr[m][n];
    for(int i=0; i<=m; i++){
        delete[] arr[i];
    }
    delete[] arr;

    return (m + n - lcs);
}

int main(){
    int t;
    cin>>t;
    while(t--){
        char str1[100], str2[100];
        cin>>str1>>str2;
        cout<<SCS(str1, str2)<<endl;
    }

    return 0;
}