class MidPoint{
    Node list;

    static class Node{
        int data;
        Node next;
        Node(int data){
            this.data = data;
            next = null;
        }
    }

    public Node createLL(int n){
        if(n==0){
            return null;
        }
        Node head = new Node(n);

        return head;
    }

    public static void main(String[] args){
        
    }
}