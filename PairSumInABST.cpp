#include<iostream>

using namespace std;

int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int target;
    cin>>target;
    int start = 0;
    int end = n-1;
    while(start<end){
        if(arr[start]+arr[end]==target){
            cout<<arr[start]<<" "<<arr[end]<<endl;
            start++;
            end--;
        }
        else if(arr[start]+arr[end] < target){
            start++;
        }
        else{
            end--;
        }
    }
    return 0;
}