#include<iostream>
#include<stack>

using namespace std;
bool isDuplicate(char str[]){
    stack<int> paranthesis, content;
    for(int i=0; str[i]!=0; i++){
        if(str[i]=='('){
            paranthesis.push(i);
        }
        else if(str[i]==')'){
            if(content.empty()){
                return true;
            }
            else{
                int x = paranthesis.top();
                if(x>content.top()){
                    return true;
                }
                while(!content.empty() && x<content.top()){
                    content.pop();
                }
                paranthesis.pop();
            }
        }
        else{
            content.push(i);
        }
    }
    return false;
}
int main(){
    int T;
    cin>>T;
    while(T--){
        char str[1000];
        cin>>str;
        if(isDuplicate(str)){
            cout<<"Duplicate"<<endl;
        }
        else{
            cout<<"Not Duplicates"<<endl;
        }
    }
    return 0;
}