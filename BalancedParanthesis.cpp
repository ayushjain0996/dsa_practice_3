#include<iostream>
#include<stack>

using namespace std;
bool isBalanced(char str[], stack<char> S){
    //Base Condition
    if(str[0]=='\0'){
        return true;
    }
    //Recursive Call
    if(str[0]=='('){
        S.push('(');
        return isBalanced(&str[1], S);
    }
    else if(str[0]=='{'){
        S.push('{');
        return isBalanced(&str[1], S);
    }
    else if(str[0]=='['){
        S.push('[');
        return isBalanced(&str[1], S);
    }
    else if(str[0]==')'){
        if(S.empty()){
            return false;
        }
        else{
            if(S.top()=='('){
                S.pop();
                return isBalanced(&str[1], S);
            }
            else{
                return false;
            }
        }
    }
    else if(str[0]=='}'){
        if(S.empty()){
            return false;
        }
        else{
            if(S.top()=='{'){
                S.pop();
                return isBalanced(&str[1], S);
            }
            else{
                return false;
            }
        }
    }
    else if(str[0]==']'){
        if(S.empty()){
            return false;
        }
        else{
            if(S.top()=='['){
                S.pop();
                return isBalanced(&str[1], S);
            }
            else{
                return false;
            }
        }
    }
    else{
        return isBalanced(&str[1], S);
    }
}
int main(){
    char str[1000];
    cin.getline(str, 1000);
    stack<char> S;
    if(isBalanced(str, S)){
        cout<<"Yes"<<endl;
    }
    else{
        cout<<"No"<<endl;
    }
    return 0;
}