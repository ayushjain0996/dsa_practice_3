#include<iostream>

using namespace std;

int max(int a, int b){
    if(a>b){
        return a;
    }
    return b;
}

int LCS(char str1[], char str2[], int m, int n){
    int lcs[101][101];
    for(int i=0; i<=m; i++){
        for(int j=0; j<=n; j++){
            if(i==0 || j==0){
                lcs[i][j] = 0;
            }
            else if(str1[i-1]==str2[j-1]){
                lcs[i][j] = lcs[i-1][j-1] + 1;
            }
            else{
                lcs[i][j] = max(lcs[i-1][j], lcs[i][j-1]);
            }
        }
    }
    return lcs[m][n];
}

int main(){
    char str1[100];
    char str2[100];
    cin>>str1>>str2;
    int m=0, n=0;
    while(str1[m]!=0){
        m++;
    }
    while(str2[n]!=0){
        n++;
    }
    cout<<LCS(str1,str2,m,n)<<endl;
    return 0;
}