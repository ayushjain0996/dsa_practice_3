#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * creatLL(int n){
    if(n==0){
        return NULL;
    }
    int x;
    cin>>x;
    node * head = new node(x);
    node * tail = head;
    n--;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
node * findAtK(node * head, int k){
    while(k--){
        head = head->next;
    }
    return head;
}
void swap(node *& head, int i, int j){
    if(i==j){
        return;
    }
    if(i==0 && j==1){
        node * a = head;
        node * b = head->next;
        node * bnext = b->next;

        head = b;
        a->next = bnext;
        head->next = a;
        return;
    }
    else if(j-i==1){
        node * aprev = findAtK(head, i-1);
        node * a = aprev->next;
        node * b = a->next;
        node * bnext = b->next;

        aprev->next = b;
        a->next = bnext;
        b->next = a;
        return;
    }
    else if(i==0){
        node * bprev = findAtK(head, j-1);
        node * a = head;
        node * b = bprev->next;
        node * bnext = b->next;

        b->next = a->next;
        head = b;
        bprev->next = a;
        a->next = bnext;
        return;
    }
    else{
        node * aprev = findAtK(head, i-1);
        node * bprev = findAtK(head, j-1);
        node * a = aprev->next;
        node * b = bprev->next;
        node * bnext = b->next;

        aprev->next = b;
        b->next = a->next;
        bprev->next = a;
        a->next = bnext;
        return;
    }
    return;
}
void SelectionSort(node *& head, int n){
    node * minNode = head;
    for(int i=0; i<n-1; i++){
        if(i){
            minNode = minNode->next;
        }
        node * currNode = minNode;
        int minPos = i;
        for(int j=i; j<n; j++){
            if((minNode->data)>(currNode->data)){
                minPos = j;
                minNode = currNode;
            }
            currNode = currNode->next;
        }
        swap(head, i, minPos);
    }
    return;
}
void PrintLL(node * head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    cout<<endl;
    return;
}
int main(){
    int n;
    cin>>n;
    node * head = creatLL(n);
    SelectionSort(head, n);
    PrintLL(head);
    return 0;
}