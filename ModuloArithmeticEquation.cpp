//	https://www.hackerrank.com/challenges/equations/problem

#include<iostream>
#include<vector>
#include<math.h>

using namespace std;

int numberOfPairs(int n){
	//Make sieve
	vector<bool> prime(n+1, true);
	for(int p=2; p*p<=n; p++){
		if(prime[p]){
			for(int i=p*p; i<=n; i+=p){
				prime[i] = false;
			}
		}
	}
	//For every prime, find its occurence in n!. Multiply all values to get result
	long long x = n;
	long long r = 1;
	for(int i=2; i<=x; i++){
		if(prime[i]){
			int k = 1;
			long long powe = 0;
			while(pow(i,k)<=x){
				powe += n/pow(i,k);
				k++;
			}
			r *= (2*powe + 1);
			r = r%1000007;
		}
	}
	return r;
}

int main(){
	int n;
	cin>>n;
	cout<<numberOfPairs(n)<<endl;
	return 0;
}