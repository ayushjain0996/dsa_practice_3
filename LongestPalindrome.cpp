#include<iostream>
#include<vector>

using namespace std;

int max(int a, int b){
    if(a>b){
        return a;
    }
    return b;
}

int LPS(char str[]){
    int n = 0;
    while(str[n]!=0){
        n++;
    }
    int ** arr = new int*[n];
    for(int i=0; i<n; i++){
        arr[i] = new int[n];
    }
    for(int j=0; j<n; j++){
        for(int i=n-1; i>=0; i--){
            if(i>j){
                arr[j][i] = 0;
            }
            else if(i==j){
                arr[j][i] = 1;
            }
            else if(str[i]==str[j]){
                arr[j][i] = arr[j-1][i+1] + 2;
            }
            else{
                arr[j][i] = max(arr[j][i+1], arr[j-1][i]);
            }
        }
    }
    int result = arr[n-1][0];
    for(int i=0; i<n; i++){
        delete[] arr[i];
    }
    delete[] arr;
    return result;
}

int main(){
    char str[1000];
    cin>>str;
    cout<<LPS(str);
    return 0;
}