#include<iostream>
#include<list>
#include<set>

using namespace std;

class Graph{
    int V;
    list<pair<int, int> > * adjList;

    public:
    Graph(int v){
        V = v;
        adjList = new list<pair<int, int> >[V+1];
    }
    void addEdge(int u, int v, int dist, bool bidir = true){
        adjList[u].push_back(make_pair(v,dist));
        if(bidir){
            adjList[v].push_back(make_pair(u,dist));
        }
        return;
    }
    void Dijkstra(int src){
        int * dist = new int[V+1];
        for(int i=1; i<=V; i++){
            dist[i] = -1;
        }

        set<pair<int, int> > s;
        dist[src] = 0;
        s.insert(make_pair(dist[src], src));

        while(!s.empty()){
            pair<int, int> curr = *(s.begin());
            int currNode = curr.second;
            int currDist = curr.first;
            s.erase(s.begin());
            
            list<pair<int, int> > :: iterator it;
            for(it=adjList[currNode].begin(); it!=adjList[currNode].end(); it++){
                pair<int, int> childPair = *it;
                int childDist = childPair.second;
                int childNode = childPair.first;
                
                if(dist[childNode]<0 || currDist + childDist < dist[childNode]){
                    set<pair<int, int> > :: iterator f;
                    f = s.find(make_pair(dist[childNode], childNode));
                    if(f!=s.end()){
                        s.erase(f);
                    }
                    
                    dist[childNode] = currDist + childDist;
                    s.insert(make_pair(dist[childNode], childNode));
                }
            }
        }
        for(int i=1; i<=V; i++){
            if(i!=src){
                cout<< dist[i] <<" ";
            }
        }
        return;
    }
};
void driverFunction(){
    int n,m;
    cin>>n>>m;
    Graph g(n);
    while(m--){
        int x,y,d;
        cin>>x>>y>>d;
        g.addEdge(x,y,d);
    }
    int src;
    cin>>src;
    g.Dijkstra(src);
    cout<<endl;
    return;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        driverFunction();
    }
    return 0;
}