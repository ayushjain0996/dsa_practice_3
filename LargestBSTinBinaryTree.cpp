#include<iostream>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
int findPosInArray(int arr[], int n, int val){
    for(int i=0; i<n; i++){
        if(arr[i] == val){
            return i;
        }
    }
    return -1;
}
node * BuildTree(int inOrder[], int preOrder[], int n){
    if(n==0){
        return NULL;
    }
    node * root = new node(preOrder[0]);
    int headPos = findPosInArray(inOrder, n, preOrder[0]);
    root->left = BuildTree(inOrder, &preOrder[1], headPos);
    root->right = BuildTree(&inOrder[headPos+1], &preOrder[headPos+1], n-headPos-1);
    return root;
}
void PrintTree(node * root){
    if(root==NULL){
        return;
    }
    cout<< root->data <<" ";
    PrintTree(root->left);
    PrintTree(root->right);
    return;
}
pair<bool , int> MaxSizeBST(node * root){
    if(root==NULL){
        pair<bool, int> p(true, 0);
        return p;
    }
    pair<bool, int> leftVal = MaxSizeBST(root->left);
    pair<bool, int> rightVal = MaxSizeBST(root->right);
    if(leftVal.first && rightVal.first){
        int lowerVal = root->data;
        int higherVal = root->data;
        node * leftNode = root->left;
        node * rightNode = root->right;
        while(leftNode->right){
            leftNode = leftNode->right;
        }
        lowerVal = leftNode->data;
        while(rightNode->left){
            rightNode = rightNode->left;
        }
        higherVal = rightNode->data;
        int greater = leftVal.second;
        if(greater < rightVal.second){
            greater = rightVal.second;
        }
        if((root->data > lowerVal) && (higherVal > root->data)){
            pair<bool, int> p(true, greater + 1);
            return p;
        }
        pair<bool, int> p(false, greater);
        return p;
    }
    else{
        int greater = leftVal.second;
        if(greater < rightVal.second){
            greater = rightVal.second;
        }
        pair<bool, int> p(false, greater);
        return p;
    }
    
}
int main(){
    int n;
    cin>>n;
    int preOrder[1000];
    int inOrder[1000];
    for(int i=0; i<n; i++){
        cin>>preOrder[i];
    }
    for(int i=0; i<n; i++){
        cin>>inOrder[i];
    }
    node * root = BuildTree(inOrder, preOrder, n);
    pair<bool , int> p = MaxSizeBST(root);
    cout<<p.second<<endl;
    cout<<endl;    

    return 0;
}