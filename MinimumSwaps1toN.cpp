//  https://www.geeksforgeeks.org/minimum-number-of-swaps-required-to-sort-an-array-of-first-n-number/?ref=leftbar-rightbar

#include<iostream>

using namespace std;

int minSwaps(int arr[], int n){
    int count = 0;
    for(int i=0; i<n; i++){
        if(arr[i] != i+1){
            while(arr[i] != i+1){
                int temp = arr[arr[i]-1];
                arr[arr[i]-1] = arr[i];
                arr[i] = temp;
                count++;
            }
        }
    }
    return count;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int * arr = new int[n];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        cout<< minSwaps(arr, n)<<endl;
        delete[] arr;
    }
    return 0;
}