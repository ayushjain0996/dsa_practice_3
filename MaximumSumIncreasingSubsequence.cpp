#include<iostream>
#include<algorithm>

using namespace std;

int MSIS(int arr[], int n){
    int * sum = new int[n];
    sum[0] = arr[0];
    for(int i=1; i<n; i++){
        sum[i] = arr[i];
        for(int j=0; j<i; j++){
            if(arr[i]>arr[j] && sum[i]<sum[j]+arr[i]){
                sum[i] = sum[j] + arr[i];
            }
        }
    }
    int result = 0;
    for(int i=0; i<n; i++){
        result = max(result, sum[i]);
    }
    delete[] sum;
    return result;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int * arr = new int[n];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        cout<<MSIS(arr,n)<<endl;
        delete[] arr;
    }
    return 0;
}