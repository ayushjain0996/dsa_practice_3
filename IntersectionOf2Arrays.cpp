#include<iostream>
#include<algorithm>
#include<map>
#include<vector>

using namespace std;

vector<int> intersection(vector<int> arr1, vector<int> arr2, int n){
    map<int, int> count;
    for(int i=0; i<n; i++){
        if(count.find(arr1[i])==count.end()){
            count[arr1[i]]=1;
        }
        else{
            count[arr1[i]]++;
        }
    }
    vector<int> intersect;
    for(int i=0; i<n; i++){
        if(count.find(arr2[i])!=count.end()){
            if(count[arr2[i]]>0){
                intersect.push_back(arr2[i]);
                count[arr2[i]]--;
            }
        }
    }
    sort(intersect.begin(), intersect.end());
    return intersect;
}

int main(){
    int n;
    cin>>n;
    vector<int> arr1, arr2;
    for(int i=0; i<n; i++){
        int x;
        cin>>x;
        arr1.push_back(x);
    }
    for(int i=0; i<n; i++){
        int x;
        cin>>x;
        arr2.push_back(x);
    }
    vector<int> intersect = intersection(arr1, arr2, n);
    for(int i=0; i<intersect.size(); i++){
        if(i==0){
            cout<<"["<<intersect[i]<<",";
        }
        else if(i==intersect.size()-1){
            cout<<" "<<intersect[i]<<"]";
        }
        else{
            cout<<" "<<intersect[i] <<",";
        }
    }

    return 0;
}