#include<iostream>
#include<stack>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = NULL;
        right = NULL;
    }
};
node * CreateTree(){
    int x;
    cin>>x;
    node * root = new node(x);
    char str[6];
    cin>>str;
    if(str[0]=='t'){
        root->left = CreateTree();
    }
    cin>>str;
    if(str[0]=='t'){
        root->right = CreateTree();
    }
    return root;
}
bool SumsUp(node * root, int sumG, int sum){
    if(root==NULL && sum==sumG){
        return true;
    }
    if(root==NULL){
        return false;
    }
    bool left = SumsUp(root->left, sumG, sum + root->data);
}
int main(){

    return 0;
}