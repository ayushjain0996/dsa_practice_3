#include<iostream>
#include<queue>
#include<stack>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
node * CreateTree(){
    int x;
    cin>>x;
    node * root = new node(x);
    char str[6];
    cin>>str;
    if(str[0]=='t'){
        root->left = CreateTree();
    }
    cin>>str;
    if(str[0]=='t'){
        root->right = CreateTree();
    }
    return root;
}
void LevelPrint(node * root){
    if(root==NULL){
        return;
    }
    stack<node *> s1;
    stack<node *> s2;
    s1.push(root);
    while(!s1.empty() || !s1.empty()){
        while(!s1.empty()){
            node * top = s1.top();
            s1.pop();
            cout<< top->data <<" ";
            if(top->left){
                s2.push(top->left);
            }
            if(top->right){
                s2.push(top->right);
            }
        }
        while(!s2.empty()){
            node * top = s2.top();
            s2.pop();
            cout<< top->data <<" ";
            if(top->right){
                s1.push(top->right);
            }
            if(top->left){
                s1.push(top->left);
            }
        }
    }
    return;
}
int main(){
    node * root = CreateTree();
    LevelPrint(root);
    return 0;
}