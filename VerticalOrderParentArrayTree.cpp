#include<iostream>
#include<vector>
#include<map>
using namespace std;

struct node{
    int data;
    node* left;
    node* right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};

void createNode(int parent[], int i, node * created[], node ** root){
    if(created[i]!=NULL){
        return;
    }

    created[i] = new node(i);

    if(parent[i]==-1){
        *root = created[i];
        return;
    }

    if(created[parent[i]]==NULL){
        createNode(parent, parent[i], created, root);
    }

    node * p = created[parent[i]];
    if(p->left==NULL){
        p->left = created[i];
    }
    else{
        p->right = created[i];
    }
    return;
}

node * createTree(int parent[], int n){
    node * created[1000];

    for(int i=0; i<n; i++){
        created[i] = NULL;
    }

    node * root = NULL;
    for(int i=0; i<n; i++){
        createNode(parent, i, created, &root);
    }

    return root;

}

void verticalOrder(node * root, int level, map<int, vector<int> > & verticalMap){
    if(root==NULL){
        return;
    }
    verticalMap[level].push_back(root->data);
    verticalOrder(root->left, level-1, verticalMap);
    verticalOrder(root->right, level+1, verticalMap);
    return;
}
void printVerticalOrder(node * root){
    map<int, vector<int> > verticalMap;
    verticalOrder(root, 0, verticalMap);
    map<int, vector<int> > :: iterator it;
    for(it=verticalMap.begin(); it!=verticalMap.end(); it++){
        int n=it->second.size();
        for(int i=0; i<n; i++){
            cout<< it->second[i] <<" ";
        }
        cout<<endl;
    }
    return;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int parent[1000];
        for(int i=0; i<n; i++){
            cin>>parent[i];
        }
        node * root = createTree(parent, n);
        printVerticalOrder(root);
    }
    return 0;
}