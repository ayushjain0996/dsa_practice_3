#include<iostream>
#include<queue>

using namespace std;

typedef pair<int, pair<int, int> > mypair;

void mergeKSortedArrays(vector<int> arr[], vector<int> output, int k, int n){
    if(n==0){
        return;
    }
    priority_queue<mypair, vector<mypair>, greater<mypair> > pq;
    int count = 0;

    for(int i=0; i<k; i++){
        pair<int, int> p1(i, 0);
        mypair p2(arr[i][0], p1);
        pq.push(p2);
    }

    while(!pq.empty()){
        mypair curr = pq.top();
        pq.pop();
        output[count++] = curr.first;
        int i = curr.second.first;
        int j = curr.second.second;

        if(j+1<n){
            pair<int, int> p1(i, j + 1);
            mypair p2(arr[i][j + 1], p1);
            pq.push(p2);
        }
    }
    return;
}

int main(){
    int n, k;
    cin>>k>>n;
    vector<int> arr[10];
    for(int i=0; i<k; i++){
        for(int j=0; j<n; j++){
            int x;
            cin>>x;
            arr[i].push_back(x);
        }
    }
    vector<int> output;
    mergeKSortedArrays(arr, output, k, n);
    int size = k*n;
    for(int i=0; i<size; i++){
        cout<<output[i] <<" ";
    }
    cout<<endl;
    return 0;
}