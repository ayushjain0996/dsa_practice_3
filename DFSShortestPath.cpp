#include<iostream>
#include<list>

using namespace std;

class Graph{
    int V;
    list<int> * adjList;

    void DFSUtil(int u, bool visited[]){
        visited[u] = true;
        cout<< u+1 <<" ";
        list<int> :: iterator it;
        for(it=adjList[u].begin(); it!=adjList[u].end(); it++){
            if(!visited[*it]){
                DFSUtil(*it, visited);
            }
        }
        return;
    }

    public:
    Graph(int v){
        V = v;
        adjList = new list<int>[V];
    }
    void addEdge(int u, int v, bool bidir = true){
        adjList[u].push_back(v);
        if(bidir){
            adjList[v].push_back(u);
        }
        return;
    }
    void DFS(int u){
        bool * visited = new bool[V]{0};
        DFSUtil(u, visited);
        cout<<endl;
        return;
    }

};

int main(){
    int n, m;
    cin>>n>>m;
    Graph g(n);
    while(m--){
        int x,y;
        cin>>x>>y;
        g.addEdge(x-1, y-1, false);
    }
    g.DFS(0);
    return 0;
}