#include<iostream>

using namespace std;

struct  node
{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
node * addNode(node * root, int val){
    if(root == NULL){
        root = new node(val);
        return root;
    }
    if(root->data > val){
        root->left = addNode(root->left, val);
    }
    else{
        root->right = addNode(root->right, val);
    }
    return root;
}
node * buildBST(int arr[], int n){
    node * root = NULL;
    for(int i=0; i<n; i++){
        root = addNode(root, arr[i]);
    }
    return root;
}

void insertDuplicates(node * root){
    if(root==NULL){
        return;
    }
    int curr = root->data;
    node * currTemp = new node(curr);
    node * temp = root->left;
    root->left = currTemp;
    currTemp->left = temp;
    insertDuplicates(root->left->left);
    insertDuplicates(root->right);
    return;
}
void printTree(node * root){
    if(root==NULL){
        return;
    }
    if(root->left){
        cout<< root->left->data <<" => ";
    }
    else{
        cout<<"END  => ";
    }
    cout<< root->data;
    if(root->right){
        cout<<" <= "<< root->right->data <<endl;
    }
    else{
        cout<<" <= END"<<endl;
    }
    printTree(root->left);
    printTree(root->right);
}

int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    node * root = buildBST(arr, n);
    insertDuplicates(root);
    printTree(root);
    return 0;
}