#include<iostream>
#include<queue>
#include<map>
using namespace std;

typedef pair<int, int> mapPair;

struct node{
    long int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};

node * createTree(){
    long int data;
    cin>>data;
    if(data==-1){
        return NULL;
    }
    node * root = new node(data);
    queue<node *> q1, q2;
    q1.push(root);
    while(!q1.empty() || !q2.empty()){
        while(!q1.empty()){
            node * top = q1.front();
            q1.pop();
            cin>>data;
            if(data!=-1){
                top->left = new node(data);
                q2.push(top->left);
            }
            cin>>data;
            if(data!=-1){
                top->right = new node(data);
                q2.push(top->right);
            }
        }
        while(!q2.empty()){
            node * top = q2.front();
            q2.pop();
            cin>>data;
            if(data!=-1){
                top->left = new node(data);
                q1.push(top->left);
            }
            cin>>data;
            if(data!=-1){
                top->right = new node(data);
                q1.push(top->right);
            }
        }
    }
    return root;
}

void bottomView(node * root, int vLevel, int hLevel, map<int, mapPair> & mb){
    if(root==NULL){
        return;
    }
    if(mb.find(vLevel)==mb.end()){
        mapPair p(hLevel, root->data);
        mb[vLevel] = p;
    }
    else{
        mapPair curr = mb[vLevel];
        if(curr.first <= hLevel){
            mapPair p(hLevel, root->data);
            mb[vLevel] = p;
        }
    }
    bottomView(root->left, vLevel - 1, hLevel + 1 ,mb);
    bottomView(root->right, vLevel + 1, hLevel + 1,mb);
    return;
}

int main(){
    node * root = createTree();
    map<int, mapPair>  mp;
    bottomView(root, 0, 0, mp);
    map<int, mapPair> :: iterator it;
    for(it=mp.begin(); it!=mp.end(); it++){
        cout<< it->second.second <<" ";
    }
    cout<<endl;
    return 0;
}