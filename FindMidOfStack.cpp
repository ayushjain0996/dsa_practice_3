#include<iostream>
#include<stack>
using namespace std;
int main(){
    int n;
    cin>>n;
    stack<int> S;
    for(int i=0; i<n; i++){
        int x;
        cin>>x;
        S.push(x);
    }
    int m=n/2;
    while(m--){
        S.pop();
    }
    cout<< S.top() <<endl;
    return 0;
}