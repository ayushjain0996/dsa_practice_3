#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <stack>

using namespace std;

void driverFunction(){
    int n;
    cin>>n;
    vector<pair<long long int,long long int> > arr(n,make_pair(0,0));
    for(int i=0; i<n; i++){
        long long int x, y;
        cin>>x>>y;
        arr[i] = make_pair(x,y);
    }
    sort(arr.begin(), arr.end());
    stack<pair<long long int, long long int> > s;
    for(int i=0; i<n; i++){
        if(s.empty()){
            s.push(arr[i]);
        }
        else{
            if(arr[i].first<=s.top().second){
                if(arr[i].second>s.top().second){
                    pair<long long int, long long int> temp = s.top();
                    s.pop();
                    temp.second = arr[i].second;
                    s.push(temp);
                }
            }
            else{
                s.push(arr[i]);
            }
        }
    }
    stack<pair<long long int, long long int> > s2;
    while(!s.empty()){
        s2.push(s.top());
        s.pop();
    }
    while(!s2.empty()){
        cout<< s2.top().first <<" "<< s2.top().second <<endl;
        s2.pop();
    }
    return;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        driverFunction();
    }
    return 0;
}