//  Determine if the array can divided into 2 subsets with equal sum.

#include<iostream>

using namespace std;

bool subset(int arr[], int n){
    int sum=0;
    for(int i=0; i<n; i++){
        sum += arr[i];
    }
    if(sum%2==1){
        return false;
    }
    sum = sum/2;
    bool ** DP = new bool*[sum+1];
    for(int i=0; i<=sum; i++){
        DP[i] = new bool[n+1];
    }
    for(int i=0; i<=sum; i++){
        for(int j=0; j<=n; j++){
            if(i==0){
                DP[i][j] = true;
            }
            else if(j==0){
                DP[i][j] = false;
            }
            else if(arr[j-1]<=i){
                DP[i][j] = DP[i-arr[j-1]][j-1] || DP[i][j-1];
            }
            else{
                DP[i][j] = DP[i][j-1];
            }
        }
    }
    int result = DP[sum][n];
    for(int i=0; i<=sum; i++){
        delete[] DP[i];
    }
    delete[] DP;
    return result;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int * arr = new int[n];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        if(subset(arr, n)){
            cout<<"YES"<<endl;
        }
        else{
            cout<<"NO"<<endl;
        }
        delete[] arr;
    }
    return 0;
}