// You are given a 2D array and each cell has some eggs in it represented by a number. you have to start at (0,0) you can either move right or left. now when you reach (m,n) you shd hav collected max eggs. WAP to print max number of eggs that can be collected in a given matrix.
#include<iostream>
#include<algorithm>

using namespace std;

int maxEggs(int ** arr, int m, int n){
    if(m==0 || n==0){
        return 0;
    }
    int * DP = new int[n+1]{0};
    for(int i=m-1; i>=0; i--){
        for(int j=n-1; j>=0; j--){
            DP[j] = arr[i][j] + max(DP[j] , DP[j+1]);
        }
    }
    int result = DP[0];
    delete[] DP;
    return result;
}

int main(){
    int m, n;
    cin>>m>>n;
    int ** arr = new int*[m];
    for(int i=0; i<m; i++){
        arr[i] = new int[n];
    }
    for(int i=0; i<m; i++){
        for(int j=0; j<n; j++){
            cin>>arr[i][j];
        }
    }
    cout<< maxEggs(arr, m, n)<<endl;
    for(int i=0; i<m; i++){
        delete[] arr[i];
    }
    delete[] arr;
    return 0;
}