#include<iostream>
#include<queue>
#include<vector>
#include<map>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};

node * createTree(){
    int data;
    cin>>data;
    if(data==-1){
        return NULL;
    }
    node * root = new node(data);
    queue<node *> q1, q2;
    q1.push(root);
    while(!q1.empty() || !q2.empty()){
        while(!q1.empty()){
            node * top = q1.front();
            q1.pop();
            cin>>data;
            if(data!=-1){
                top->left = new node(data);
                q2.push(top->left);
            }
            cin>>data;
            if(data!=-1){
                top->right = new node(data);
                q2.push(top->right);
            }
        }
        while(!q2.empty()){
            node * top = q2.front();
            q2.pop();
            cin>>data;
            if(data!=-1){
                top->left = new node(data);
                q1.push(top->left);
            }
            cin>>data;
            if(data!=-1){
                top->right = new node(data);
                q1.push(top->right);
            }
        }
    }
    return root;
}

void verticalOrder(node * root, int level, map<int, vector<int> > & mymap){
    if(root==NULL){
        return;
    }
    mymap[level].push_back(root->data);
    verticalOrder(root->left, level-1, mymap);
    verticalOrder(root->right, level+1, mymap);
    return;
}
void printVerticalOrder(node * root){
    map<int, vector<int> > mymap;
    verticalOrder(root, 0, mymap);
    map<int, vector<int> > :: iterator it;
    for(it=mymap.begin(); it!=mymap.end(); it++){
        for(int i=0; i < it->second.size(); i++){
            cout<< it->second[i] <<" ";
        }
        cout<<endl;
    }
    return;
}

int main(){
    int x;
    cin>>x;
    node * root = createTree();
    printVerticalOrder(root);
    return 0;
}