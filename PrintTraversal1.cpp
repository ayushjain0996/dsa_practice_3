#include<iostream>
#include<list>
#include<queue>

using namespace std;

class Graph{
    int V;
    list<int> * adjList;

    public:
    Graph(int v){
        V = v;
        adjList = new list<int>[V + 1];
    }
    void addEdge(int u, int v, bool bidir = true){
        adjList[u].push_back(v);
        if(bidir){
            adjList[v].push_back(u);
        }
        return;
    }
    void bfs(int src){
        queue<int> q;
        bool * visited = new bool[V+1]{0};

        q.push(src);
        visited[src] = true;
        while(!q.empty()){
            int node = q.front();
            q.pop();
            cout<< node <<" ";
            list<int> :: iterator it;
            for(it=adjList[node].begin(); it!=adjList[node].end(); it++){
                int neighbour = *it;
                if(!visited[neighbour]){
                    q.push(neighbour);
                    visited[neighbour] = true;
                }
            }
        }
    }
};
void buildAndPrintTree(){
    int n;
    cin>>n;
    Graph g(n);
    int u,v;
    cin>>u>>v;
    g.addEdge(u,v);
    n = n-2;
    while(n--){
        int x, y;
        cin>>x>>y;
        g.addEdge(x,y);
    }
    g.bfs(1);
    return;
}

int main(){
    buildAndPrintTree();
    return 0;
}