#include<iostream>
#include<map>
#include<list>

using namespace std;

template<typename T>
class Graph{
    map<T, list<T> > adjList;

    Graph(){

    }

    public:
    void addEdge(T u, T v, bool bidir = true){
        adjList[u].push_back(v);
        if(bidir){
            adjList[v].push_back(u);
        }
    }
    void printAdjList(){
        map<T, list<T> > :: iterator itMap;
        for(itMap = adjList.begin(); itMap != adjList.end(); itMap++){
            cout<< itMap.first <<"-> ";
            list<T> :: iterator itList;
            list<T> currList = itMap.second;
            for(itList = currList.begin(); itList!=currList.end(); itList++){
                cout<< (*itList) <<", ";
            }
            cout<<endl;
        }
    }
};

int main(){
    Graph<int> g;

    g.addEdge(0, 1);
    g.addEdge(0,2);

    return 0;
}