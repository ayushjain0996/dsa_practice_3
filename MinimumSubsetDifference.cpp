//  https://practice.geeksforgeeks.org/problems/minimum-sum-partition/0

#include<iostream>
#include<vector>
#include<algorithm>

using namespace std;

int minDiff(int arr[], int n){
    int sum = 0;
    for(int i=0; i<n; i++){
        sum += arr[i];
    }
    int range = sum;
    sum /= 2;
    int minVal = range;
    vector<vector<bool> > DP(sum+1, vector<bool>(n+1, false));
    for(int i=0; i<=sum; i++){
        for(int j=0; j<=n; j++){
            if(i==0){
                DP[0][j] = true;
            }
            else if(j==0){
                DP[i][0] = false;
            }
            else if (i<arr[j-1])
            {       
                DP[i][j] = DP[i][j-1]; 
            }
            else
            {
                DP[i][j] = DP[i-arr[j-1]][j-1] || DP[i][j-1];
            }
            if(DP[i][j]){
                int currVal = range - 2*i;
                minVal = min(currVal, minVal);
            }
        }
    }
    
    return minVal;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int * arr = new int[n];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        cout<< minDiff(arr, n) <<endl;
        delete[] arr;
    }
    return 0;
}