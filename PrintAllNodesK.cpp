#include<iostream>
using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};

void printKdistanceNodesDown(node * root, int k){
    if(root==NULL){
        return;
    }
    if(k<0){
        return;
    }
    if(k==0){
        cout<< root->data <<" ";
    }

    printKdistanceNodesDown(root->left, k-1);
    printKdistanceNodesDown(root->right, k-1);
    return;
}
int printKDistanceNodes(node * root, int target, int k){
    if(root == NULL){
        return -1;
    }
    if(root->data == target){
        printKdistanceNodesDown(root, k);
        return 0;
    }
    int dLeft = printKDistanceNodes(root->left, target, k);
    if(dLeft!=-1){
        if(dLeft+1==k){
            cout<< root->data <<" ";
        }
        else{
            printKdistanceNodesDown(root->right, k-dLeft-2);
        }
        return 1+dLeft;
    }
    int dRight = printKDistanceNodes(root->right, target, k);
    if(dRight!=-1){
        if(dRight+1==k){
            cout<< root->data <<" ";
        }
        else{
            printKdistanceNodesDown(root->left, k -dRight-2);
        }
        return 1+dRight;
    }
    return -1;
}
int findPosition(int n, int arr[], int k){
    for(int i=0; i<n; i++){
        if(arr[i]==k){
            return i;
        }
    }
    return -1;
}
node * buildBST(int n, int preOrder[], int inOrder[]){
    if(n<=0){
        return NULL;
    }
    node * root = new node(preOrder[0]);
    int rootPosition = findPosition(n, inOrder, preOrder[0]);
    root->left = buildBST(rootPosition, &preOrder[1], inOrder);
    root->right = buildBST(rootPosition, &preOrder[rootPosition+1], &inOrder[rootPosition+1]);
    return root;
}
int main(){
    int n;
    cin>>n;
    int preOrder[1000], inOrder[1000];
    for(int i=0; i<n; i++){
        cin>>preOrder[i];
    }
    for(int i=0; i<n; i++){
        cin>>inOrder[i];
    }
    node * root = buildBST(n, preOrder, inOrder);
    int t;
    cin>>t;
    while(t--){
        int target, k;
        cin>>target>>k;
        printKDistanceNodes(root, target, k);
        cout<<endl;
    }
    return 0;
}