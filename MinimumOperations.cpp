#include<iostream>
#include<algorithm>
#include<climits>

using namespace std;

int minOperations(int n){
    int * arr = new int[n+1];
    for(int i=0; i<n; i++){
        arr[i] = INT_MAX;
    }
    arr[n] = 0;
    for(int i=n; i>0; i--){
        if(i%2==0){
            if(arr[i]+1<arr[i/2]){
                arr[i/2] = arr[i] + 1;
            }
            if(arr[i]+1 < arr[i-1]){
                arr[i-1] = arr[i] + 1;
            }
        }
        else{
            if(arr[i]+1 < arr[i-1]){
                arr[i-1] = arr[i] + 1;
            }
        }
    }
    int result = arr[0];
    delete[] arr;
    return result;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        cout<<minOperations(n)<<endl;
    }
    return 0;
}