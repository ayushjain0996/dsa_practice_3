#include<iostream>
#include<queue>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        this->left = NULL;
        this->right = NULL;
    }
};
node * CreateTree(){
    int x;
    cin>>x;
    node * root = new node(x);

    char str[6];
    cin>>str;
    if(str[0]=='t'){
        root->left = CreateTree();
    }
    cin>>str;
    if(str[0]=='t'){
        root->right = CreateTree();
    }
    return root;
}
void printlevel(node * root){
    if(root==NULL){
        return;
    }
    queue<node *> q1;
    queue<node *> q2;
    q1.push(root);
    cout<<"[";
    while(!q1.empty() || !q2.empty()){
        int i = 0;
        if(!q1.empty()){
            cout<<"[";
            while(!q1.empty()){
                node * top = q1.front();
                q1.pop();
                if(i!=0){
                    cout<<", "<< top->data;
                }
                else{
                    cout<< top->data;
                }
                if(top->left){
                    q2.push(top->left);
                }
                if(top->right){
                    q2.push(top->right);
                }
                i++;
            }
            cout<<"]";
        }
        if(!q2.empty()){
            cout<<", ";
        }
        i = 0;
        if(!q2.empty()){
            cout<<"[";
            while(!q2.empty()){
                node * top = q2.front();
                q2.pop();
                if(i!=0){
                    cout<<", "<< top->data;
                }
                else{
                    cout<< top->data;
                }
                if(top->left){
                    q1.push(top->left);
                }
                if(top->right){
                    q1.push(top->right);
                }
                i++;
            }
            cout<<"]";
        }
        if(!q1.empty()){
            cout<<", ";
        }
    }
    cout<<"]";
}
int main(){
    node * root = CreateTree();
    printlevel(root);
    return 0;
}