#include<iostream>
#include<vector>
#include<queue>

using namespace std;

struct cell{
    int x;
    int y;
    int dis;
    cell(){}
    cell(int x, int y, int dis){
        this->x = x;
        this->y = y;
        this->dis = dis;
    }
};

bool isInside(int x, int y, int N){
    return (x>=0 && x<N && y>=0 && y<N);
}

int minStepsNeeded(int startRow, int startCol, int endRow, int endCol, int N){
    int dx[] = {-2, -2, 2, 2, -1, -1, 1, 1};
    int dy[] = {-1, 1, -1, 1, -2, 2, -2, 2};

    queue<cell> q;
    q.push(cell(startRow, startCol, 0));
    vector<vector<bool> > vis(N, vector<bool>(N, false));

    while(!q.empty()){
        cell t = q.front();
        q.pop();

        if(t.x==endRow && t.y==endCol){
            return t.dis;
        }

        for(int i=0; i<8; i++){
            int x = t.x + dx[i];
            int y = t.y + dy[i];

            if(isInside(x, y, N) && !vis[x][y]){
                vis[x][y] = true;
                q.push(cell(x, y, t.dis + 1));
            }
        }
    }
    return -1;
}

int main(){
    int N;
    cin>>N;
    int startRow, startCol;
    cin>>startRow>>startCol;
    int endRow, endCol;
    cin>>endRow>>endCol;
    cout<<minStepsNeeded(startRow, startRow, endRow, endCol, N)<<endl;

    return 0;
}