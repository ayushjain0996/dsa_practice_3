#include<iostream>
#include<vector>

using namespace std;

int max(int a, int b){
    if(a>b){
        return a;
    }
    return b;
}

//k eggs, n floors
int eggDrop(int n, int k){
    if(n==1 || n==0){
        return n;
    }
    if(k==1){
        return n;
    }
    int minVal = INT_MAX;
    for(int x=1; x<=n; x++){
        int curr = max(eggDrop(x-1, k-1), eggDrop(n-x, k));
        if(minVal>curr){
            minVal = curr;
        }
    }
    return minVal + 1;
}

//k floors, n eggs
int eggDropDP(int k, int n){
    vector<vector<int> > eggFloor;
    int res;
    
    for(int i=0; i<=n; i++){
        vector<int> v;
        eggFloor.push_back(v);
    }
    //Base Cases
    for(int i=0; i<=n; i++){
        eggFloor[i].push_back(0);
        eggFloor[i].push_back(1);
    }
    for(int j=2; j<=k; j++){
        eggFloor[1].push_back(j);
    }
    for(int i=2; i<=n; i++){
        for(int j=2; j<=k; j++){
            eggFloor[i].push_back(INT_MAX);
            for(int x=1; x<=j; x++){
                res = 1 + max(eggFloor[i-1][x-1], eggFloor[i][i-x]);
                if(res<eggFloor[i][j]){
                    eggFloor[i][j] = res;
                }
            }
        }
    }
    return eggFloor[n-1][k-1];
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n, k;
        cin>>k>>n;
        cout<<eggDrop(n,k)<<endl; 
        cout<<eggDropDP(n,k)<<endl;
    }

    return 0;
}