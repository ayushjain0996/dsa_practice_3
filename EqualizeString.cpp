#include<iostream>
#include<algorithm>

using namespace std;

int editDistance(char str1[], char str2[], int m, int n){
    int ** arr = new int*[m+1];
    for(int i=0; i<=m; i++){
        arr[i] = new int[n+1];
    }
    for(int i=0; i<=m; i++){
        for(int j=0; j<=n; j++){
            if(i==0){
                arr[i][j] = j;
            }
            else if(j==0){
                arr[i][j] = i;
            }
            else if(str1[i-1]==str2[j-1]){
                arr[i][j] = arr[i-1][j-1];
            }
            else{
                arr[i][j] = 1 + min(arr[i-1][j-1], min(arr[i-1][j], arr[i][j-1]));
            }
        }
    }
    int result = arr[m][n];
    delete[] arr;
    return result;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int m,n;
        cin>>m>>n;
        char * str1 = new char[m+1];
        char * str2 = new char[n+1];
        cin>>str1>>str2;
        cout<<editDistance(str1, str2, m, n)<<endl;
        delete[] str1;
        delete[] str2;
    }
    return 0;
}