#include<iostream>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right =NULL;
    }
};

node * addNode(node * root, int val){
    if(root==NULL){
        root = new node(val);
        return root;
    }
    if(root->data > val){
        root->left = addNode(root->left, val);
    }
    else{
        root->right = addNode(root->right, val);
    }
    return root;
}

node * buildBST(int arr[], int n){
    if(n==0){
        return NULL;
    }
    node * root = NULL;
    for(int i=0; i<n; i++){
        root = addNode(root, arr[i]);
    }
    return root;
}
void printKeysInRange(node * root, int k1, int k2){
    if(root==NULL){
        return;
    }
    if(root->data > k1){
        printKeysInRange(root->left, k1, k2);
    }
    if(root->data >= k1 && root->data <=k2){
        cout<< root->data <<" ";
    }
    if(root->data < k2){
        printKeysInRange(root->right, k1, k2);
    }
    return;
}

void printPreOrder(node * root){
    if(root==NULL){
        return;
    }
    cout<< root->data <<" ";
    printPreOrder(root->left);
    printPreOrder(root->right);
    return;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int arr[1000];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        int k1, k2;
        cin>>k1>>k2;
        node * root = buildBST(arr, n);
        cout<<"# Preorder : ";
        printPreOrder(root);
        cout<<"\n# Nodes within range are : ";
        printKeysInRange(root, k1, k2);
        cout<<endl;
    }
    
    return 0;
}