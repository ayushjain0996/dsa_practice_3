#include<iostream>

using namespace std;

int countWays(int n){
    int arr[3];
    for(int i=0; i<=n; i++){
        if(i==0 || i==1){
            arr[i] = 1;
        }
        else if(i==2){
            arr[i] = 2;
        }
        else{
            arr[i%3] = arr[(i-1)%3] + arr[(i-2)%3] + arr[(i-3)%3];
        }
    }
    int result = arr[n%3];
    return result;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        cout<<countWays(n)<<endl;
    }
    return 0;
}