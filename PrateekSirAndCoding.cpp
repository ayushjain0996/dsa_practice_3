#include<iostream>
#include<stack>

using namespace std;

int main(){
    int n;
    cin>>n;
    stack<int> S;
    while(n--){
        int x1, x2;
        cin>>x1;
        if(x1==2){
            cin>>x2;
            S.push(x2);
        }
        else{
            if(S.empty()){
                cout<<"No Code"<<endl;
            }
            else{
                cout<<S.top()<<endl;
                S.pop();
            }
        }
    }
    return 0;
}