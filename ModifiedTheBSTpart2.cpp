#include<iostream>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};

node * addNode(node * root, int val){
    if(root == NULL){
        root = new node(val);
        return root;
    }
    if(root->data > val){
        root->left = addNode(root->left, val);
    }
    else{
        root->right = addNode(root->right, val);
    }
    return root;
}
node * buildBST(int arr[], int n){
    node * root = NULL;
    for(int i=0; i<n; i++){
        root = addNode(root, arr[i]);
    }
    return root;
}

void printPreOrder(node * root){
    if(root==NULL){
        return;
    }
    cout<< root->data <<" ";
    printPreOrder(root->left);
    printPreOrder(root->right);
    return;
}

node * removeOutsideRange(node * root, int min, int max){
    if(root==NULL){
        return NULL;
    }

    root->left = removeOutsideRange(root->left, min, max);
    root->right = removeOutsideRange(root->right, min, max);

    if(root->data < min){
        node * rChild = root->right;
        delete root;
        return rChild;
    }
    if(root->data > max){
        node * lChild = root->left;
        delete root;
        return lChild;
    }
    return root;
}


int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int arr[1000];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        node * root = buildBST(arr, n);
        cout<<"Preorder : ";
        printPreOrder(root);
        cout<<endl;
        int k1, k2;
        cin>>k1>>k2;
        root = removeOutsideRange(root, k1, k2);
        cout<<"Preorder : ";
        printPreOrder(root);
        cout<<endl;
    }
    return 0;
}