#include<iostream>
#include<queue>
#include<vector>

using namespace std;

struct cell{
    int x;
    int y;
    int dis;
    cell(int x, int y, int dis){
        this->x = x;
        this->y = y;
        this->dis = dis;
    }
};

bool isInside(int x, int y, int n, int m){
    return (x>=0 && x<n && y>=0 && y<m);
}

int shortestPath(int ** arr, int n, int m, int endX, int endY){
    int dx[] = {-1, -1, 1, 1};
    int dy[] = {-1, 1, -1, 1};

    queue<cell> q;
    q.push(cell(0,0,0));

    vector<vector<bool> > vis(n, vector<bool>(m, false));
    vis[0][0] = true;
    while (!q.empty())
    {
        cell top = q.front();
        q.pop();

        if(top.x == endX && top.y==endY){
            return top.dis;
        }

        for(int i=0; i<4; i++){
            int currx = top.x + dx[i];
            int curry = top.y + dy[i];

            if(isInside(currx,curry,n,m) && !vis[currx][curry] && arr[currx][curry]==1){
                vis[currx][curry] = true;
                q.push(cell(currx, curry, top.dis + 1));
            }
        }
    }
    return -1;
}

int main(){

    int n, m;
    cin>>m>>n;
    int ** arr = new int*[n];
    for(int i=0; i<n; i++){
        arr[i] = new int[m];
    }
    for(int i=0; i<n; i++){
        for(int j=0; j<m; j++){
            cin>> arr[i][j];
        }
    }
    int x,y;
    cin>>x>>y;
    cout<< shortestPath(arr, n, m, x, y) << endl;
    for(int i=0; i<n; i++){
        delete[] arr[i];
    }
    delete[] arr;
    return 0;
}