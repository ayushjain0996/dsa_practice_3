#include<iostream>
#include<queue>
#include<map>
using namespace std;

typedef pair<int, int> mypair;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};

node * createTree(){
    int data;
    cin>>data;
    if(data==-1){
        return NULL;
    }
    node * root = new node(data);
    queue<node *> q1, q2;
    q1.push(root);
    while(!q1.empty() || !q2.empty()){
        while(!q1.empty()){
            node * top = q1.front();
            q1.pop();
            cin>>data;
            if(data!=-1){
                top->left = new node(data);
                q2.push(top->left);
            }
            cin>>data;
            if(data!=-1){
                top->right = new node(data);
                q2.push(top->right);
            }
        }
        while(!q2.empty()){
            node * top = q2.front();
            q2.pop();
            cin>>data;
            if(data!=-1){
                top->left = new node(data);
                q1.push(top->left);
            }
            cin>>data;
            if(data!=-1){
                top->right = new node(data);
                q1.push(top->right);
            }
        }
    }
    return root;
}

void topView(node * root, int vLevel, int hLevel, map<int, mypair> & mt){
    if(root==NULL){
        return;
    }
    if(mt.find(vLevel) == mt.end()){
        mypair p(hLevel, root->data);
        mt[vLevel] = p;
    }
    else{
        mypair curr = mt[vLevel];
        if(curr.first > hLevel){
            mypair p(hLevel, root->data);
            mt[hLevel] = p;
        }
    }
    topView(root->left, vLevel-1, hLevel+1, mt);
    topView(root->right, vLevel+1, hLevel+1, mt);
    return;
}

int main(){
    node * root = createTree();
    map<int, mypair>  mt;
    topView(root, 0, 0, mt);
    map<int, mypair> :: iterator it;
    for(it=mt.begin(); it!=mt.end(); it++){
        cout<< it->second.second <<" ";
    }
    cout<<endl;
    return 0;
}