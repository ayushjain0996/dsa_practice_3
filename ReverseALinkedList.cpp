#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * CreateLL(int n){
    if(n==0){
        return NULL;
    }
    n--;
    int x;
    cin>>x;
    node * head = new node(x);
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
void PrintLL(node * head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    cout<<endl;
    return;
}
void Reverse(node *& head){
    if(head==NULL || head->next==NULL){
        return;
    }
    if(head->next->next==NULL){
        head->next->next = head;
        head = head->next;
        head->next->next = NULL;
        return;
    }
    node * point1 = head;
    node * point2 = point1->next;
    node * point3 = point2->next;
    point1->next = NULL;
    while(point3){
        point2->next = point1;
        point1 = point2;
        point2 = point3;
        point3 = point3->next;   
    }
    point2->next = point1;
    head = point2;
    return;
}
int main(){
    int n;
    cin>>n;
    node * head = CreateLL(n);
    Reverse(head);
    PrintLL(head);
    return 0;
}