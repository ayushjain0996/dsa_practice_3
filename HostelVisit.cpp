#include<iostream>
#include<queue>

using namespace std;

class Hostels{
    int hostelCount;    //HostelCount checks if the count of hostel is less than or greater than k
    priority_queue<long long int> host;   //Priority Queue stores rocket distances of k nearest hostels only.
    
    public:    
    Hostels(){
        hostelCount = 0;
    }

    void addHostel(long long int x, long long int y, int k){
        long long int dist = x*x + y*y;
        if(hostelCount<k){
            host.push(dist);
            hostelCount++;
        }
        else{
            long long int kthDist = host.top();
            if(kthDist>dist){
                host.pop();
                host.push(dist);
            }
            hostelCount++;
        }
    }
    int kThNearest(){
        return host.top();
    }

};

void driverFunction(int n, int k){
    Hostels h;
    while(n--){
        int q;
        cin>>q;
        if(q==1){
            long long int x, y;
            cin>>x>>y;
            h.addHostel(x,y,k);
        }
        else{
            cout<<h.kThNearest()<<endl;
        }
    }
}

int main(){
    int n, k;
    cin>>n>>k;
    driverFunction(n,k);
    return 0;
}