#include<iostream>
#include<list>
#include<queue>

using namespace std;

class Graph{
    int V;
    list<int> * adjList;

    public:
    Graph(int v){
        V = v;
        adjList = new list<int>[V];
    }
    void addEdge(int u, int v, bool bidir = true){
        adjList[u].push_back(v);
        if(bidir){
            adjList[v].push_back(u);
        }
        return;
    }
    void bfs(int src=0, int dest=99){
        queue<int> q;
        bool * visited = new bool[V]{0};
        int * distance = new int[V];
        for(int i=0; i<V; i++){
            distance[i] = -1;
        }

        q.push(src);
        visited[src] = true;
        distance[src] = 0;
        while(!q.empty()){
            int node = q.front();
            q.pop();
            list<int> :: iterator it;
            for(it=adjList[node].begin(); it!=adjList[node].end(); it++){
                int neighbour = *it;
                if(!visited[neighbour]){
                    q.push(neighbour);
                    visited[neighbour] = true;
                    distance[neighbour] = distance[node] + 1;
                }
            }
        }
        cout<< distance[dest] <<endl;
    }
};

void playGame(){
    int board[100] = {0};
    Graph g(100);
    int l;
    cin>>l;
    while(l--){
        int x, y;
        cin>>x>>y;
        board[x-1] = y-x;
    }
    int s;
    cin>>s;
    while(s--){
        int x,y;
        cin>>x>>y;
        board[x-1] = y-x;
    }
    for(int u=0; u<100; u++){
        for(int dice = 1; dice<=6; dice++){
            if(u+dice<=99){
                int v = u + dice + board[u+dice];
                g.addEdge(u, v, false);
            }
        }
    }
    g.bfs();
    return;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        playGame();
    }
    return 0;
}