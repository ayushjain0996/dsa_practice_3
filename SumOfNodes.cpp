#include<iostream>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = NULL;
        right = NULL;
    }
};
node * CreateTree(){
    int x;
    cin>>x;
    node * root = new node(x);
    char str[6];
    cin>>str;
    if(str[0]=='t'){
        root->left = CreateTree();
    }
    cin>>str;
    if(str[0]=='t'){
        root->right = CreateTree();
    }
    return root;
}
int SumOfNodes(node * root){
    if(root == NULL){
        return 0;
    }
    int left = SumOfNodes(root->left);
    int right = SumOfNodes(root->right);
    int self = root->data;
    return left + right + self;
}
int main(){
    node * root = CreateTree();
    cout<< SumOfNodes(root) <<endl;
    return 0;
}