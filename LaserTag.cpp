#include<iostream>

using namespace std;

int numOfMatches(int n){
    if(n==2){
        return 1;
    }
    if(n%2==0){
        return numOfMatches(n/2) + 1;
    }
    else{
        return numOfMatches(n/2+1) + 1;
    }
}

bool isPossible(int n, int x){
    int matches = numOfMatches(n);
    if(matches%2==0){
        return (x >= 0.5*matches);
    }
    else{
        return (x > 0.5*matches);
    }
}

int main(){
    int n, x;
    cin>>n>>x;
    cout<< numOfMatches(n) <<endl;
    cout<<isPossible(n, x)<<endl;
    return 0;
}