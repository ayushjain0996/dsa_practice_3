#include<iostream>
#include<algorithm>

using namespace  std;

int coinsChange(int coins[], int n, int amt){
    int ** arr = new int*[amt+1];
    for(int i=0; i<=amt; i++){
        arr[i] = new int[n+1];
    }
    for(int i=0; i<=amt; i++){
        for(int j=0; j<=n; j++){
            if(i==0){
                arr[i][j] = 1;
            }
            else if(j==0){
                arr[i][j] = 0;
            }
            else if(coins[j-1]<=i){
                arr[i][j] = arr[i-coins[j-1]][j] + arr[i][j-1];
            }
            else{
                arr[i][j] = arr[i][j-1];
            }
        }
    }
    int result = arr[amt][n];
    return result;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int * coins = new int[n];
        for(int i=0; i<n; i++){
            cin>>coins[i];
        }
        int amt;
        cin>>amt;
        cout<<coinsChange(coins, n, amt)<<endl;
    }
    return 0;
}