#include<iostream>
#include<stack>

using namespace std;

int main(){
    int n;
    cin>>n;
    stack<int> S;
    for(int i=0; i<n; i++){
        int num;
        cin>>num;
        int span = 1;
        stack<int> s1;
        s1.push(num);
        while(!S.empty() && num>S.top()){
            int x = S.top();
            S.pop();
            s1.push(num);
            span++;
        }
        while(!s1.empty()){
            int x = s1.top();
            s1.pop();
            S.push(x);
        }
        cout<<span<<" ";
    }
    cout<<"END"<<endl;
    return 0;
}