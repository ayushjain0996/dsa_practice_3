#include<iostream>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};

node* addNode(node * root, int val){
    if(root==NULL){
        root = new node(val);
        return root;
    }
    if(root->data < val){
        root->right = addNode(root->right, val);
    }
    else{
        root->left = addNode(root->left, val);
    }
    return root;
}

node * buildBST(int arr[], int n){
    node * root = NULL;
    for(int i=0; i<n; i++){
        root = addNode(root, arr[i]);
    }
    return root;
}
node * LCA(node * root, int v1, int v2){
    if(root==NULL){
        return NULL;
    }
    if(root->data == v1){
        return root;
    }
    if(root->data == v2){
        return root;
    }
    node * leftNode = LCA(root->left, v1, v2);
    node * rightNode = LCA(root->right, v1, v2);
    if(leftNode==NULL && rightNode==NULL){
        return NULL;
    }
    if(leftNode!=NULL && rightNode!=NULL){
        return root;
    }
    return (leftNode!=NULL) ? leftNode : rightNode;
}

void printBST(node * root){
    if(root==NULL){
        return;
    }
    cout<< root->data <<" ";
    printBST(root->left);
    printBST(root->right);
    return;
}

int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    node * root = buildBST(arr, n);
    int v1, v2;
    cin>>v1>>v2;
    node * lcaNode = LCA(root, v1, v2);
    if(lcaNode){
        cout<< lcaNode->data <<endl;
    }
    else{
        cout<<"-1"<<endl;
    }
    return 0;
}