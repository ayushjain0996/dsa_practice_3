#include<iostream>
#include<set>
#include<list>

using namespace std;

class Graph{
    int V;
    list<pair<int, int> > * adjList;

    public:
    Graph(int v){
        V = v;
        adjList = new list<pair<int, int> >[V];
    }
    void addEdge(int u, int v, int prob, bool bidir = true){
        adjList[u].push_back(make_pair(v,prob));
        if(bidir){
            adjList[v].push_back(make_pair(u, prob));
        }
        return;
    }
    void maxProb(int src, int dest){
        double * prob = new double[V];
        for(int i=0; i<V; i++){
            prob[i] = 0.0;
        }

        set<pair<double, int> > s;
        prob[src] = -1.0;
        s.insert(make_pair(-1.0, src));

        while(!s.empty()){
            pair<int, int> curr = *(s.begin());
            int node = curr.second;

            s.erase(s.begin());

            list<pair<int, int> > :: iterator it;
            for(it=adjList[node].begin(); it!=adjList[node].end(); it++){
                pair<int, int> childPair = *it;
                int childNode = childPair.first;
                int childProb = childPair.second;
                if(prob[node]*childProb*0.01 < prob[childNode]){
                    set<pair<double, int> > :: iterator f;
                    f = s.find(make_pair(prob[childNode], childNode));

                    if(f!=s.end()){
                        s.erase(f);
                    }
                    prob[childNode] = prob[node]*childProb*0.01;
                    s.insert(make_pair(prob[childNode], childNode));
                }
            }
        }
        cout<< prob[dest] <<endl;
    }
};

void findProbability(){
    int n, t;
    cin>>n>>t;
    Graph g(n);
    while(t--){
        int x,y,p;
        cin>>x>>y>>p;
        g.addEdge(x-1,y-1,p);
    }
    int zero;
    cin>>zero;
    g.maxProb(0,n-1);
    return;
}
int main(){
    findProbability();
    return 0;
}