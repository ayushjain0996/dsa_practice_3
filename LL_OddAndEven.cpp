#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * createLL(int n){
    if(n==0){
        return NULL;
    }
    int x;
    cin>>x;
    node * head = new node(x);
    n--;
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
void printLL(node * head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    return;
}
void OddEven(node * head, int n){
    if(n<3){
        return;
    }
    if(n%2==1){
        node * tail1 = head;
        node * element = head;
        while(tail1->next){
            tail1 = tail1->next;
        }
        node * tail2 = tail1;
        while(element!=tail1){
            tail2->next = element->next;
            element->next = element->next->next;
            tail2 = tail2->next;
            tail2->next = NULL;
            element = element->next;
        }
        return;
    }
    else{
        node * tail1 = head;
        node * element = head;
        while(tail1->next->next){
            tail1 = tail1->next;
        }
        node * tail2 = tail1;
        node * tail = tail1->next;
        while(element!=tail1){
            tail2->next = element->next;
            element->next = element->next->next;
            tail2 = tail2->next;
            tail2->next = tail;
            element = element->next;
        }
        return;
    }
}
int main(){
    int n;
    cin>>n;
    node * head = createLL(n);
    OddEven(head, n);
    printLL(head);
    return 0;
}
