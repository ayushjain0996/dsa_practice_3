#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * CreateLL(int n){
    if(n==0){
        return NULL;
    }
    int x;
    cin>>x;
    n--;
    node * head = new node(x);
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
void PrintLL(node * head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    cout<<endl;
    return;
}
void removeDuplicates(node * head){
    node * aprev = head;
    node * a = aprev->next;
    while(a){
        if( (aprev->data)==(a->data) && (a->next==NULL)){
            aprev->next = NULL;
            delete a;
            return;
        }
        if((aprev->data)==(a->data)){
            aprev->next = a->next;
            node * temp = a;
            a = a->next;
            delete temp;
        }
        else{
            aprev=a;
            a=a->next;
        }
    }
    return;
}
int main(){
    int T;
    cin>>T;
    while(T--){
        int n;
        cin>>n;
        node * head = CreateLL(n);
        removeDuplicates(head);
        PrintLL(head);
    }
    return 0;
}