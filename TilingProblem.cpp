#include<iostream>

using namespace std;

int numOfWays(int n){
    int * DP = new int[n+1]{0};
    DP[0] = 1;
    DP[1] = 1;
    for(int i=2; i<=n; i++){
        DP[i] = DP[i-1] + DP[i-2];
    }
    int result = DP[n];
    delete[] DP;
    return result;
}

int main(){
    int n;
    cin>>n;
    cout<< numOfWays(n) <<endl;
    return 0;
}