#include<iostream>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};

node * buildBST(){
    int val;
    int k;
    cin>>val>>k;
    node * root = new node(val);
    if(k==0){
        return root;
    }
    else if(k==1){
        root->left = buildBST();
        return root;
    }
    else{
        root->left = buildBST();
        root->right = buildBST();
        return root;
    }
}

void updateSumAtK(node * root, int level, int * sum){
    if(root==NULL){
        return;
    }
    if(level==0){
        *sum += root->data;
        return;
    }
    updateSumAtK(root->left, level-1, sum);
    updateSumAtK(root->right, level-1, sum);
    return;

}

int getSumAtK(node * root, int k){
    int sum = 0;
    updateSumAtK(root, k, &sum);
    return sum;
}

void printTree(node * root){
    if(root==NULL){
        return;
    }
    cout<< root->data <<" ";
    printTree(root->left);
    printTree(root->right);
    return;
}

int main(){
    node * root = buildBST();
    int k;
    cin>>k;
    cout<<getSumAtK(root, k)<<endl;
    return 0;
}