#include<iostream>
#include<list>
#include<set>

using namespace std;

class Graph{
    int V;
    list<pair<int, int> > * adjList;

    public:
    Graph(int v){
        V = v;
        adjList = new list<pair<int, int> >[V];
    }
    void addEdge(int u, int v, int dist, bool bidir = true){
        adjList[u].push_back(make_pair(v,dist));
        if(bidir){
            adjList[v].push_back(make_pair(u, dist));
        }
        return;
    }
    void dijkstraSP(int src){
        int * dist = new int[V];
        for(int i=0; i<V; i++){
            dist[i] = INT_MAX;
        }

        set<pair<int, int> > s;
        dist[src] = 0;
        s.insert(make_pair(0,src));

        while(!s.empty()){
            pair<int, int> curr = *(s.begin());
            int node = curr.second;
            int nodeDist = curr.first;

            s.erase(s.begin());

            list<pair<int, int> > :: iterator it;
            for(it=adjList[node].begin(); it!=adjList[node].end(); it++){
                pair<int, int> childPair = *it;
                if(nodeDist + childPair.second < dist[childPair.first]){
                    int dest = childPair.first;

                    set<pair<int, int> > :: iterator f;
                    f = s.find(make_pair(dist[dest], dest));
                    if(f!=s.end()){
                        s.erase(f);
                    }
                    
                    dist[dest] = nodeDist + childPair.second;
                    s.insert(make_pair(dist[dest], dest));
                }
            }
        }
        for(int i=0; i<V; i++){
            cout<<"Distance of node "<<src<<" "<<" to node "<<i<<" is "<<dist[i]<<endl;
        }

    }

};

int main(){
    Graph g(5);
    g.addEdge(1,2,1);
    g.addEdge(1,3,4);
    g.addEdge(2,3,1);
    g.addEdge(3,4,2);
    g.addEdge(1,4,7);
    g.dijkstraSP(1);
    return 0;
}