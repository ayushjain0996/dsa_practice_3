#include<iostream>
#include<climits>
#include<algorithm>

using namespace std;

int numberOfDrops(int n, int k){
    int ** DP = new int*[n+1];
    for(int i=0; i<=n; i++){
        DP[i] = new int[k+1];
    }
    for(int i=0; i<=n; i++){
        for(int j=0; j<=k; j++){
            if(i==0 || j==0){
                DP[i][j] = 0;
            }
            else if(i==0){
                DP[i][j] = 1;
            }
            else if(j==1){
                DP[i][j] = i;
            }
            else{
                DP[i][j] = INT_MAX;
                for(int x=1; x<=i; x++){
                    int curr = max(DP[x-1][j-1], DP[i-x][j]);
                    if(curr<DP[i][j]){
                        DP[i][j] = curr + 1;
                    }
                }
            }
        }
    }
    int result = DP[n][k];
    for(int i=0; i<=n; i++){
        delete[] DP[i];
    }
    delete[] DP;
    return result;
}

int main(){
    int t;
    cin>>t;
    while (t--)
    {
        int n, k;
        cin>>k>>n;
        cout<<numberOfDrops(n, k)<<endl;
    }
    return 0;
}