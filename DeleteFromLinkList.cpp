#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * CreateLL(int n){
    if(n==0){
        return NULL;
    }
    n--;
    int x;
    cin>>x;
    node * head = new node(x);
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
void PrintLL(node * head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    cout<<endl;
    return;
}
void DeleteAtK(node *& head, int k){
    if(k==0){
        node * temp = head;
        head = head->next;
        delete temp;
    }
    else{
        node * temp = head;
        k--;
        while(k--){
            temp = temp->next;
        }
        node * temp1 = temp->next;
        temp->next = temp->next->next;
        delete temp1;
    }
    return;
}
int main(){
    int n, q;
    cin>>n>>q;
    node * head = CreateLL(n);
    while(q--){
        int k;
        cin>>k;
        DeleteAtK(head, k);
        PrintLL(head);
    }
    return 0;
}