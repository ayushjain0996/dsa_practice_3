#include<iostream>
#include<stack>

using namespace std;
struct queue{
    stack<int> s1;  //primary stack
    stack<int> s2;  //helper stack

    void push(int data){
        while(!s1.empty()){
            s2.push(s1.top());
            s1.pop();
        }
        s1.push(data);
        while(!s2.empty()){
            s1.push(s2.top());
            s2.pop();
        }
        return;
    }
    void pop(){
        s1.pop();
        return;
    }
    int front(){
        return s1.top();
    }
    bool empty(){
        return s1.empty();
    }
};
int main(){
    int n;
    cin>>n;
    queue Q;
    for(int i=0; i<n; i++){
        Q.push(i);
    }
    while(!Q.empty()){
        cout<< Q.front() << " ";
        Q.pop();
    }
    cout<<endl;
    return 0;
}