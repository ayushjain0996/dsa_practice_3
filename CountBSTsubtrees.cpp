#include<iostream>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
node * buildBST(int inOrder[], int start, int end){
    if(start>end){
        return NULL;
    }
    int mid = (start + end)/2;
    node * root = new node(inOrder[mid]);

    root->left = buildBST(inOrder, start, mid-1);
    root->right = buildBST(inOrder, mid+1, end);
    return root;
}
bool updateCount(node * root, int min, int max, int * count){
    if(root==NULL){
        return true;
    }
    bool l = updateCount(root->left, min, max ,count);
    bool r = updateCount(root->right, min, max, count);
    bool curr = (root->data >= min) && (root->data <= max);
    if(l && r && curr){
        cout<< *count <<endl;
        return true;
    }
    return false;
}
int getCountOfNodes(node * root, int min, int max){
    int count = 0;
    updateCount(root, min, max, &count);
    return count;
}

int main(){
    int n;
    cin>>n;
    int inOrder[1000];
    for(int i=0; i<n; i++){
        cin>>inOrder[i];
    }
    int k1, k2;
    cin>>k1>>k2;
    node * root = buildBST(inOrder, 0, n);
    cout<<getCountOfNodes(root, k1, k2) <<endl;
    return 0;
}