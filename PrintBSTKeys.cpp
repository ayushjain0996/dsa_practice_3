#include<iostream>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
void insertNode(node * root, int data){
    if(root==NULL){
        return;
    }
    if(root->data < data){
        if(root->right){
            insertNode(root->right, data);
        }
        else{
            root->right = new node(data);
        }
        return;
    }
    else{
        if(root->left){
            insertNode(root->left, data);
        }
        else{
            root->left = new node(data);
        }
    }
}
node * BuildBST(int arr[], int n){
    if(n==0){
        return NULL;
    }
    node * root = new node(arr[0]);
    for (int i = 1; i < n; i++)
    {
        insertNode(root, arr[i]);
    }
    return root;    
}
void PrintPreorder(node * root){
    if(root==NULL){
        return;
    }
    cout<<root->data<<" ";
    PrintPreorder(root->left);
    PrintPreorder(root->right);
    return;
}
int main(){
    int n;
    int arr[50];
    cin>>n;
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int x1, x2;
    cin>>x1>>x2;
    node * root = BuildBST(arr, n);
    cout<<"# Preorder : ";
    PrintPreorder(root);
    return 0;
}