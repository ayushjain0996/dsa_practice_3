#include<iostream>
#include<queue>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
node * CreateTree(){
    int x;
    cin>>x;
    node * root = new node(x);
    char str[6];
    cin>>str;
    if(str[0]=='t'){
        root->left = CreateTree();
    }
    cin>>str;
    if(str[0]=='t'){
        root->right = CreateTree();
    }
    return root;
}
void LevelPrint(node * root){
    if(root==NULL){
        return;
    }
    queue<node *> q1;
    queue<node *> q2;
    q1.push(root);
    while(!q1.empty() || !q2.empty()){
        
        while(!q1.empty()){
            node * top = q1.front();
            q1.pop();
            cout<< top->data <<" ";
            if(top->left){
                q2.push(top->left);
            }
            if(top->right){
                q2.push(top->right);
            }
        }
        if(!q2.empty()){
            cout<<endl;
        }
        while(!q2.empty()){
            node * top = q2.front();
            q2.pop();
            cout<< top->data <<" ";
            if(top->left){
                q1.push(top->left);
            }
            if(top->right){
                q1.push(top->right);
            }
        }
        cout<<endl;
    }
}
int main(){
    node * root = CreateTree();
    LevelPrint(root);
    return 0;
}