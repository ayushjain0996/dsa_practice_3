#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * createLL(int n){
    if(n==0){
        return NULL;
    }
    int x;
    cin>>x;
    n--;
    node * head = new node(x);
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
node * findAtK(node * head, int k){
    while(k--){
        head = head->next;
    }
    return head;
}
bool isPalindrome(node * head, int n){
    //Base Condition
    if(n<=1){
        return true;
    }
    //Recursive Call
    node * tail = findAtK(head, n-1);
    if((head->data)==(tail->data)){
        return isPalindrome(head->next, n-2);
    }
    else
    {
        return false;
    }
    
}
int main(){
    int n;
    cin>>n;
    node * head = createLL(n);
    if(isPalindrome(head, n)){
        cout<<"true"<<endl;
    }
    else{
        cout<<"false"<<endl;
    }
    return 0;
}