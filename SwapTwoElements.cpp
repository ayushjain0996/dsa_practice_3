#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * CreateLL(int n){
    if(n==0){
        return NULL;
    }
    n--;
    int x;
    cin>>x;
    node * head = new node(x);
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
void PrintLL(node * head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    cout<<endl;
    return;
}
node * findData(node * head,int data){
    while(head->data != data){
        head = head->next;
    }
    return head;
}
void swap(node *& head, int data1, int data2){
    if(data1==data2){
        return;
    }
    node * a = findData(head, data1);
    node * b = findData(head, data2);
    if(a==head && b==head->next){
        node * bnext = b->next;

        head = b;
        b->next = a;
        a->next = bnext;
        return;
    }
    else if(a->next == b){
        node * aprev = head;
        while(aprev->next!=a){
            aprev = aprev->next;
        }
        node * bnext = b->next;

        aprev->next = b;
        b->next = a;
        a->next = bnext;
        return;
    }
    else if(a==head){
        node * bprev = head;
        while(bprev->next != b){
            bprev = bprev->next;
        }
        node * bnext = b->next;

        head = b;
        b->next = a->next;
        bprev->next = a;
        a->next = bnext;
        return;
    }
    else{
        node * aprev = head;
        node * bprev = head;
        while(aprev->next != a){
            aprev = aprev->next;
        }
        while(bprev->next != b){
            bprev = bprev->next;
        }
        node * bnext = b->next;

        aprev->next = b;
        b->next = a->next;
        bprev->next = a;
        a->next = bnext;
        return;
    }
    return;
}
int main(){
    int n;
    cin>>n;
    node * head = CreateLL(n);
    int data1, data2;
    cin>>data1>>data2;
    swap(head, data1, data2);
    PrintLL(head);
    return 0;
}