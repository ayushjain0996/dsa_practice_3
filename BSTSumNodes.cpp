#include<iostream>
#include<queue>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};

node * addNode(node * root, int val){
    if(root==NULL){
        root = new node(val);
        return root;
    }
    if(root->data > val){
        root->left = addNode(root->left, val);
    }
    else{
        root->right = addNode(root->right, val);
    }
    return root;
}

node * createTree(){
    node * root = NULL;
    int data;
    cin>>data;
    while(data!=-1){
        root = addNode(root, data);
        cin>>data;
    }
    return root;
}
void printLevelOrder(node * root){
    if(root==NULL){
        return;
    }
    queue<node *> q1, q2;
    q1.push(root);
    while(!q1.empty() || !q2.empty()){
        if(!q1.empty()){
            cout<<endl;
        }
        while(!q1.empty()){
            node * top = q1.front();
            q1.pop();
            cout<< top->data <<" ";
            if(top->left){
                q2.push(top->left);
            }
            if(top->right){
                q2.push(top->right);
            }
        }
        if(!q2.empty()){
            cout<<endl;
        }
        while(!q2.empty()){
            node * top = q2.front();
            q2.pop();
            cout<< top->data <<" ";
            if(top->left){
                q1.push(top->left);
            }
            if(top->right){
                q1.push(top->right);
            }
        }
    }
    return;
}

void greaterSum(node * root, int * sum){
    if(root==NULL){
        return;
    }
    greaterSum(root->right, sum);
    *sum = *sum + root->data;
    root->data = *sum - root->data;
    greaterSum(root->left, sum);
    return;
}
void modifyBST(node * root){
    int sum = 0;
    greaterSum(root, &sum);
    return;
}

int main(){
    node * root = createTree();
    modifyBST(root);
    printLevelOrder(root);
    return 0;
}