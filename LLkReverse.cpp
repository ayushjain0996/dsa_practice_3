#include<iostream>

using namespace std;

struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        next = NULL;
    }
};

node * createLL(int n){
    if(n==0){
        return NULL;
    }
    int data;
    cin>>data;
    node * head = new node(data);
    node * curr = head;
    n--;
    while(n--){
        cin>>data;
        curr->next = new node(data);
        curr = curr->next;
    }
    return head;
}

node * kReverse(node * head, int k){
    if(head==NULL){
        return NULL;
    }
    int count = 0;
    node * curr = head;
    node * prev = NULL;
    node * next = NULL;

    while (curr!=NULL && count<k)
    {
        next = curr->next;
        curr->next = prev;
        prev = curr;
        curr = next;
        count++;
    }
    head->next = kReverse(next, k);
    return prev;    
}

void printLL(node * head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    cout<<endl;
    return;
}

int main(){
    int n, k;
    
    cin>>n>>k;
    node * head = createLL(n);
    
    head = kReverse(head, k);
    printLL(head);
    return 0;
}