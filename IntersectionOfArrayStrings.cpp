#include<iostream>
#include<unordered_map>
#include<string>
#include<vector>

using namespace std;

vector<string> intersection(string arr1[], int n1, string arr2[], int n2){
    unordered_map<string, int> mymap;
    for(int i=0; i<n1; i++){
        mymap[arr1[i]]++;
    }
    vector<string> result;
    for(int i=0; i<n2; i++){
        if(mymap.find(arr2[i]) != mymap.end() && mymap[arr2[i]]>0){
            result.push_back(arr2[i]);
            mymap[arr1[i]]--;
        }
    }
    return result;
}

int main(){
    int n1, n2;
    cin>>n1>>n2;
    string arr1[1000];
    string arr2[1000];
    for(int i=0; i<n1; i++){
        cin>>arr1[i];
    }
    for(int j=0; j<n2; j++){
        cin>>arr2[j];
    }
    vector<string> result = intersection(arr1, n1, arr2, n2);
    for(int i=0; i<result.size(); i++){
        cout<<result[i]<<" ";
    }
    cout<<endl;
    return 0;
}