#include<iostream>
#include<list>
#include<queue>

using namespace std;

class Graph{
    int V;
    list<int> * adjList;

    public:
    Graph(int v){
        V = v;
        adjList = new list<int>[V];
    }
    void addEdge(int u, int v, bool bidir = true){
        adjList[u].push_back(v);
        if(bidir){
            adjList[v].push_back(u);
        }
        return;
    }
    void bfs(int src, int dest){
        queue<int> q;
        bool * visited = new bool[V]{0};
        int * distance = new int[V]{0};

        q.push(src);
        visited[src] = true;

        while(!q.empty()){
            int node = q.front();
            q.pop();

            list<int> :: iterator it;
            for(it=adjList[node].begin(); it!=adjList[node].end(); it++){
                int neighbour = *it;
                if(!visited[neighbour]){
                    visited[neighbour] = true;
                    distance[neighbour] = distance[node] + 1;
                    q.push(neighbour);
                }
            }            
        }
        cout<<distance[dest]<<endl;
    }
};
void playGame(int n, int l, int s){
    Graph g(n+6);
    int board[56] = {0};
    while(l--){
        int x, y;
        cin>>x>>y;
        board[x-1] = y-x;
    }
    while(s--){
        int x, y;
        cin>>x>>y;
        board[x-1] = y-x;
    }

    for(int u=0; u<n; u++){
        for(int dice=1; dice<=6; dice++){
            int v = u + dice + board[u+dice];
            g.addEdge(u,v,false);
        }
    }
    g.bfs(0,n);
    return;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n, l, s;
        cin>>n>>l>>s;
        playGame(n, l, s);
    }
    return 0;
}