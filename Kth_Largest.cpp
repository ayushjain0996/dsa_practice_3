#include<iostream>
#include<queue>

using namespace std;

int KthLargest(int arr[], int n, int k){
    for(int i=0; i<k; i++){
        for(int j=1; j<n-i; j++){
            if(arr[j-1]>arr[j]){
                int temp = arr[j];
                arr[j] = arr[j-1];
                arr[j-1] = temp;
            }
        }
    }
    return arr[n-k];
}

int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int k;
    cin>>k;
    cout<<KthLargest(arr, n, k)<<endl;
    return 0;
}