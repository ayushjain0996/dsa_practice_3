#include<iostream>
#include<stack>

using namespace std;
void joinQueue(stack< pair<int, int> > &line){
    int course, rollno;
    cin>>course>>rollno;
    pair<int, int> student;
    student.first = course;
    student.second = rollno;
    stack< pair<int, int> > temp;
    while(!line.empty() && ((line.top()).first!=student.first)){
        pair<int, int> enemy = line.top();
        line.pop();
        temp.push(enemy);
    }
    if(line.empty()){
        while(!temp.empty()){
        pair<int, int> enemy = temp.top();
        temp.pop();
        line.push(enemy);
        }
        line.push(student);
    }
    else{
        line.push(student);
        while(!temp.empty()){
        pair<int, int> enemy = temp.top();
        temp.pop();
        line.push(enemy);
        }
    }
    return;
}
void leaveQueue(stack< pair<int, int> > &line){
    stack< pair<int, int> > temp;
    while(!line.empty()){
        pair<int, int> enemy = line.top();
        line.pop();
        temp.push(enemy);
    }
    pair<int, int> student = temp.top();
    temp.pop();
    while(!temp.empty()){
        pair<int, int> enemy = temp.top();
        temp.pop();
        line.push(enemy);
    }
    cout<< student.first <<" "<< student.second<<endl;
    return;
}
int main(){    
    stack< pair<int, int> > line;
    int Q;
    cin>>Q;
    while(Q--){
        char task;
        cin>>task;
        if(task=='E'){
            joinQueue(line);
        }
        else{
            leaveQueue(line);
        }
    }
    return 0;
}