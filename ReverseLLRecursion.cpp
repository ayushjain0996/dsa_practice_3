#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * creatLL(int n){
    if(n==0){
        return NULL;
    }
    n--;
    int x;
    cin>>x;
    node * head = new node(x);
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
node * reverse(node * p1, node * p2){
    //Base Condition
    if(p2==NULL){
        return p1;
    }
    //Recursive Call
    p1->next = NULL;
    node * result = reverse(p2, p2->next);
    p2->next = p1;
    return result;
}
void PrintLL(node * head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    cout<<endl;
    return;
}
int main(){
    int n;
    cin>>n;
    node * head = creatLL(n);
    node * result = reverse(head, head->next);
    PrintLL(result);
    return 0;
}