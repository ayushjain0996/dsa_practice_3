//  https://www.spoj.com/problems/CHUNK2/
#include<iostream>
#include<vector>
#include<stack>

using namespace std;

bool isPrime(int n){
    for(int i=2; i<=n/2; i++){
        if(n%i==0){
            return false;
        }
    }
    return true;
}

int getKthPrime(int k){
    int count = 0;
    int currPrime = 2;
    int i = 2;
    while(count<k){
        if(isPrime(i)){
            currPrime = i;
            count++;
        }
        i++;
    }
    return currPrime;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n,m;
        cin>>n>>m;
        vector<int> temp;
        vector<vector<int> > adj(n+1, temp);
        for(int i=0; i<m; i++){
            int u,v;
            cin>>u>>v;
            adj[u].push_back(v);
            adj[v].push_back(u);
        }
        vector<bool> vis(n+1, false);
        int maxK = 0;
        for(int i=1; i<=n; i++){
            if(!vis[i]){
                int temp = 0;
                stack<int> s;
                s.push(i);
                while(!s.empty()){
                    int t = s.top();
                    s.pop();
                    if(!vis[t]){
                        temp++;
                        vis[t] = true;
                    }
                    for(int curr=0; curr<adj[t].size(); curr++){
                        if(!vis[adj[t][curr]]){
                            s.push(adj[t][curr]);
                        }
                    }
                }
                if(maxK<temp){
                    maxK = temp;
                }
            }
        }
        if(maxK==1){
            cout<<"-1"<<endl;
        }
        else{
            int kthPrime = getKthPrime(maxK);
            cout<<kthPrime<<endl;
        }
    }
    return 0;
}