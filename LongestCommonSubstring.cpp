#include<iostream>

using namespace std;

int max(int a, int b){
    if(a>b){
        return a;
    }
    return b;
}

int LCS(char str1[], char str2[], int m, int n){
    int ** DP = new int*[m+1];
    for(int i=0; i<=m; i++){
        DP[i] = new int[n+1];
    }

    int result = 0;

    for(int i=0; i<=m; i++){
        for(int j=0; j<=n; j++){
            if(i==0 || j==0){
                DP[i][j] = 0;
            }
            else if(str1[i-1]==str2[j-1]){
                DP[i][j] = DP[i-1][j-1] + 1;
                result = max(result, DP[i][j]);
            }
            else{
                DP[i][j] = 0;
            }
        }
    }

    return result;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int m,n;
        cin>>m>>n;
        char str1[100];
        char str2[100];
        cin>>str1>>str2; 
        cout<< LCS(str1, str2, m, n)<<endl;
    }
    return 0;
}