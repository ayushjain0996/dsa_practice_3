
#include<iostream>
#include<vector>
#include<queue>

using namespace std;

bool isInside(int x, int y, int N, int M){
    return (x>=0 && x<N && y>=0 && y<M);
}

int numberOfIslands(vector<vector<char>>& grid){
    if(grid.size() || grid[0].size()){
        return 0;
    }

    int N = grid.size();
    int M = grid.size();
    int answer = 0;
    vector<vector<bool> > vis(N, vector<bool>(M, false));
    
    int dx[] = {0, 0, -1, 1};
    int dy[] = {-1, 1, 0, 0};

    for(int i=0; i<N; i++){
        for(int j=0; j<M; j++){
            if(!vis[i][j] && grid[i][j]=='1'){
                answer++;
                queue<pair<int, int> > q;
                q.push(make_pair(i, j));
                vis[i][j] = true;
                
                while(!q.empty()){
                    pair<int, int> t = q.front();
                    q.pop();
                    for(int k=0; k<4; k++){
                        int currx = t.first + dx[k];
                        int curry = t.second + dy[k];
                        if(isInside(currx, curry, N, M) && !vis[currx][curry]){
                            vis[currx][curry] = true;
                            q.push(make_pair(currx, curry));
                        }
                    }
                }
            }
        }
    }
    return answer;
}

int main(){
    int N, M;
    cin>>N>>M;
    vector<vector<char> > grid(N, vector<char>(M, '0'));
    for(int i=0; i<N; i++){
        for(int j=0; j<M; j++){
            char x;
            cin>>x;
            grid[i][j] = x;
        }
    }
    cout<<numberOfIslands(grid)<<endl;
    return 0;
}