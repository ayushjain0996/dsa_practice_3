#include<iostream>
#include<queue>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
void preOrder(node * root){
    if(root==NULL){
        return;
    }
    cout<< root->data <<" ";
    preOrder(root->left);
    preOrder(root->right);
    return;
}

void inOrder(node * root){
    if(root==NULL){
        return;
    }
    inOrder(root->left);
    cout<<root->data<<" ";
    inOrder(root->right);
    return;
}

void postOrder(node * root){
    if(root==NULL){
        return;
    }
    postOrder(root->left);
    postOrder(root->right);
    cout<<root->data <<" ";
    return;
}

void levelOrder(node * root){
    if(root==NULL){
        return;
    }
    queue<node *> q;
    q.push(root);
    while(!q.empty()){
        //while(q.size()>0)
        node * curr = q.front();
        q.pop();
        
        cout<< curr->data <<" ";

        if(curr->left!=NULL){
            q.push(curr->left);
        }
        if(curr->right){
            q.push(curr->right);
        }
    }
    return;
}

//PreOrder
//Input -> 5 0 3 0 2 -1 -1 0 1 -1 -1 0 4 0 6 -1 -1 0 7 -1 -1
node * createTree(){
    node * root;
    int val;
    cin>>val;
    root = new node(val);
    int x;
    cin>>x;
    if(x!=-1){
        root->left = createTree();
    }
    cin>>x;
    if(x!=-1){
        root->right = createTree();
    }
    return root;
}

int main(){
    node * root = createTree();

    preOrder(root);
    cout<<endl;
    inOrder(root);
    cout<<endl;
    postOrder(root);
    cout<<endl;
    levelOrder(root);
    cout<<endl;
    return 0;
}