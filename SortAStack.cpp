#include<iostream>
#include<stack>
using namespace std;
void StackSort(stack<int> &S){
    //Base Condition
    if(S.empty()){
        return;
    }
    //Recursive Call
    int x = S.top();
    S.pop();
    StackSort(S);
    stack<int> s1;
    while(!S.empty() && x<S.top()){
        int y = S.top();
        S.pop();
        s1.push(y);
    }
    S.push(x);
    while(!s1.empty()){
        int y=s1.top();
        s1.pop();
        S.push(y);
    }
    return;
}
int main(){
    int n;
    cin>>n;
    stack<int> S;
    while(n--){
        int x;
        cin>>x;
        S.push(x);
    }
    StackSort(S);
    while(!S.empty()){
        cout<< S.top() <<" ";
        S.pop();
    }
    return 0;
}