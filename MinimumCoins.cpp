#include<iostream>
#include<queue>

using namespace std;
void minimumCoins(int n){
    int arr[] = {1,2,5,10,20,50,100,200,500,2000};
    priority_queue<pair<int, int> > pq;
    
    int it = sizeof(arr)/sizeof(int) - 1;
    int currVal = n;

    while(currVal>0){
        while(arr[it]>currVal && it>=0){
            it--;
        }
        pair<int, int> currPair;
        currPair.first = arr[it];
        currPair.second = currVal/arr[it];
        currVal = currVal%arr[it];
        pq.push(currPair);
    }
    while(!pq.empty()){
        pair<int, int> currPair = pq.top();
        pq.pop();
        for(int i=0; i<currPair.second; i++){
            cout<<currPair.first<<" ";
        }
    }
    cout<<endl;
    return;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        minimumCoins(n);
    }
    return 0;
}