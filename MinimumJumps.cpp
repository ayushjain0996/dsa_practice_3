#include<iostream>
#include<algorithm>

using namespace std;

int minJumps(int arr[], int n){
    int * jumps = new int[n];
    for(int i=0; i<n; i++){
        jumps[i] = n;
    }    
    jumps[n-1] = 0;
    for(int i=n-2; i>=0; i--){
        int minVal = n;
        for(int j=i+1; j<=min(i+arr[i], n-1); j++){
            minVal = min(minVal, 1+jumps[j]);
        }
        jumps[i] = minVal;
    }
    if(jumps[0]>=n){
        jumps[0] = -1;
    }
    int result = jumps[0];
    delete[] jumps;
    return result;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        int * arr = new int[n];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        cout<<minJumps(arr, n)<<endl;
        delete[] arr;
    }
    return 0;
}