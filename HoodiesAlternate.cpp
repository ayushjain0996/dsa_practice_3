#include<iostream>
#include<queue>

using namespace std;
bool isPresent(queue<int> line, int i){
    while(!line.empty()){
        if(line.front()==i){
            return true;
        }
        line.pop();
    }
    return false;
}
void joinQueue(queue<int> course[], queue<int> &line, int x, int y){
    if(isPresent(line, x)){
        course[x-1].push(y);
    }
    else{
        line.push(x);
        course[x-1].push(y);
    }
}
void leaveQueue(queue<int> course[], queue<int> &line){
    int x = line.front();
    int y = course[x-1].front();
    course[x-1].pop();
    if(course[x-1].empty()){
        line.pop();
    }
    cout<<x<<" "<<y<<endl;
    return;
}
int main(){
    queue<int> course[4];
    queue<int> line;
    int Q;
    cin>>Q;
    while(Q--){
        char x;
        cin>>x;
        if(x=='E'){
            int x, y;
            cin>>x>>y;
            joinQueue(course, line, x, y);
        } 
        else{
            leaveQueue(course, line);
        }
    }
    return 0;
}