#include<iostream>
#include<algorithm>

using namespace std;

int maxScore(int arr[], int n){
    int ** DP = new int*[n];
    for(int i=0; i<n; i++){
        DP[i] = new int[n];
    }

    for(int i=n-1; i>=0; i--){
        for(int j=0; j<n; j++){
            if(i>j){
                DP[i][j] = 0;
            }
            else if(i==j){
                DP[i][j] = arr[i];
            }
            else if(i==j-1){
                DP[i][j] = max(arr[i], arr[j]);
            }
            else{
                DP[i][j] = max(arr[i] + min(DP[i+1][j-1], DP[i+2][j]), arr[j] + min(DP[i+1][j-1], DP[i][j-2]));
            }
        }
    }

    int result = DP[0][n-1];
    for(int i=0; i<n; i++){
        delete[] DP[i];
    }
    delete[] DP;
    return result;
}

int main(){
    int t;
    cin>>t;
    while (t--){    
        int n;
        cin>>n;
        int * arr = new int[n];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        cout<<maxScore(arr, n)<<endl;
        delete[] arr;
    }
    return 0;
}