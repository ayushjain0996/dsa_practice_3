#include<iostream>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
void insertNode(node * root, int data){
    if(root==NULL){
        return;
    }
    if(root->data <= data){
        if(root->right==NULL){
            root->right = new node(data);
        }
        else{
            insertNode(root->right, data);
        }
    }
    else{
        if(root->left==NULL){
            root->left = new node(data);
        }
        else{
            insertNode(root->left, data);
        }
    }
    return;
}
node * BuildBST(int arr[], int n){
    node * root = new node(arr[0]);
    for(int i=1; i<n; i++){
        insertNode(root, arr[i]);
    }
    return root;
}
void DeleteNode(node *& root, int data){
    if(root==NULL){
        return;
    }
    if(root->data==data){
        if(root->left==NULL){
            node * temp = root;
            root = root->right;
            delete temp;
            return;
        }
        else if(root->right==NULL){
            node * temp = root;
            root = root->left;
            delete temp;
            return;
        }
        else{
            node * head = root;
            node * successor = root->right;
            if(successor->left==NULL){
                root = successor;
                successor->left = head->left;
                delete head;
                return;
            }
            while(successor->left->left){
                successor = successor->left;
            }
            node * SLeft = successor->left->right;
            
            root = successor->left;
            successor->left = SLeft;
            root->left = head->left;
            root->right = head->right;
            delete head;
            return;
            
        }
    }
    if(root->data < data){
        DeleteNode(root->right, data);
    }
    else{
        DeleteNode(root->left, data);
    }
    return;
}
void printTree(node * root){
    if(root==NULL){
        return;
    }
    cout<< root->data <<" ";
    printTree(root->left);
    printTree(root->right);
    return;
}
int main(){
    int T;
    cin>>T;
    while(T--){
        int n1;
        cin>>n1;
        int arr[1000];
        for(int i=0; i<n1; i++){
            cin>>arr[i];
        }
        node * root = BuildBST(arr, n1);
        cin>>n1;
        for(int i=0; i<n1; i++){
            cin>>arr[i];
            DeleteNode(root, arr[i]);
        }
        printTree(root);
        cout<<endl;
    }
    return 0;
}