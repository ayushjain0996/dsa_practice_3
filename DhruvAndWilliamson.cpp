#include<iostream>
#include<vector>

using namespace std;

vector<int> removeElement(vector<int> arr, int val){
    int n = arr.size();
    int pos = 0;
    for(int i=0; i<n; i++){
        if(arr[i]==val){
            pos = i;
        }
    }
    for(int j=pos; j<n-1; j++){
        arr[j] = arr[j+1];
    }
    arr.pop_back();
    return arr;
}

class Score{
    int HighScore;
    int LowScore;
    int HighCount;
    int LowCount;
    vector<int> scoreList;
    
    public:
    Score(){
        HighScore = -1;
        LowScore = 100000;
        HighCount = 0;
        LowCount = 0;
    }

    void addScore(int score){
        if(LowScore>score){
            LowScore = score;
            LowCount++;
        }
        else if(LowScore==score){
            LowCount++;
        }
        if(HighScore<score){
            HighScore = score;
            HighCount++;
        }
        else if(HighScore==score){
            HighCount++;
        }
        
        scoreList.push_back(score);
        return;
    }
    int Diff(){
        if(scoreList.empty()){
            return -1;
        }
        int diff = HighScore - LowScore;
        if(HighScore==LowScore){
            scoreList = removeElement(scoreList, HighScore);
            HighCount--;
            LowCount--;
        }
        else{
            scoreList = removeElement(scoreList, HighScore);
            scoreList = removeElement(scoreList, LowScore);
            HighCount--;
            LowCount--;
        }
        HighScore = 0;
        LowScore = 100001;
        int n = scoreList.size();
        for(int i=0; i<n; i++){
            if(scoreList[i]>HighScore){
                HighScore = scoreList[i];
            }
            if(scoreList[i]<LowScore){
                LowScore = scoreList[i];
            }
        }
        return diff;
    }
    int countHigh(){
        if(scoreList.empty()){
            return -1;
        }
        return HighCount;
    }
    int countLow(){
        if(scoreList.empty()){
            return -1;
        }
        return LowCount;
    }

};
bool isCountLow(char str1[]){
    char str2[] = "CountLow";
    for(int i=0; str1[i]!=0; i++){
        if(str1[i]!=str2[i]){
            return false;
        }
    }
    return true;
}
void driverFunction(int n){\
    Score s;
    while(n--){
        char str[10];
        cin>>str;
        if(str[0]=='P'){
            int x;
            cin>>x;
            s.addScore(x);
        }
        else if(str[0]=='D'){
            cout<<s.Diff()<<endl;
        }
        else{
            if(isCountLow(str)){
                cout<<s.countLow()<<endl;
            }
            else{
                cout<<s.countHigh()<<endl;
            }
        }
    }
}

int main(){
    int n;
    cin>>n;
    driverFunction(n);
    return 0;
}