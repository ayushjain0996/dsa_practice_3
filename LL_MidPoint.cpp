#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * CreateLL(int n){
    if(n==0){
        return NULL;
    }
    n--;
    node * head;
    int x;
    cin>>x;
    head = new node(x);
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
node * MidPoint(node * head, int n){
    int k=n/2;
    while(k>1){
        head = head->next;
        k--;
    }
    if(n%2==0){
        return head;
    }
    return head->next;
}
int main(){
    int n;
    cin>>n;
    node * head = CreateLL(n);
    cout<< MidPoint(head, n)->data <<endl;
    return 0;
}