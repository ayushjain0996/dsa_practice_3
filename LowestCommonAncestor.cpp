#include<iostream>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
node * CreateTree(){
    int x;
    cin>>x;
    node * root = new node(x);
    char str[6];
    cin>>str;
    if(str[0]=='t'){
        root->left = CreateTree();
    }
    cin>>str;
    if(str[0]=='t'){
        root->right = CreateTree();
    }
    return root;
}
bool isPresent(node * root, int data){
    if(root==NULL){
        return false;
    }
    if(root->data == data){
        return true;
    }
    bool left = isPresent(root->left, data);
    bool right = isPresent(root->right, data);
    return left || right;
}
node * LCA(node * root, int d1, int d2){
    //Base Condition
    if(root==NULL){
        return NULL;
    }
    if(root->data==d1 || root->data==d2){
        return root;
    }
    //Recursive Call
    bool leftd1 = isPresent(root->left, d1);
    bool leftd2 = isPresent(root->left, d2);
    if(leftd1==true && leftd2==true){
        return LCA(root->left, d1, d2);
    }
    else if(leftd1^leftd2==true){
        return root;
    }
    else{
        return LCA(root->right, d1, d2);
    }
}
int main(){
    node * root = CreateTree();
    int d1, d2;
    cin>>d1>>d2;
    cout<< LCA(root, d1, d2)->data <<endl;
    return 0;
}