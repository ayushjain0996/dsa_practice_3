#include<iostream>
#include<list>

using namespace std;

class Graph{
    int V;
    list<int> * adjList;

    public:
    Graph(int v){
        V = v;
        adjList = new list<int>[V];
    }
    void addEdge(int u, int v, bool bidir = true){
        adjList[u].push_back(v);
        if(bidir){
            adjList[v].push_back(u);
        }
        return;
    }
    int beautifulVertices(){
        bool * visited = new bool[V]{0};
        int count = 0;
        for(int i=0; i<V; i++){
            if(!visited[i]){
                DFS(i,-1,visited,count);
            }
        }
        return count;
    }
    void DFS(int curr, int parent, bool visited[], int &count){
        visited[curr] = true;
        list<int> :: iterator it;
        for(it=adjList[curr].begin(); it!=adjList[curr].end(); it++){
            int child = *it;
            if(child!=parent){
                DFS(child, curr, visited, count);
                int children = adjList[curr].size();
                if(parent!=-1){
                    children--;
                }
                int grandChildren = adjList[child].size() - 1;
                if(grandChildren>children){
                    count++;
                }
            }
        }
        return;
    }
};

void driverFunction(){
    int n, m;
    cin>>n>>m;
    Graph g(n);
    while(m--){
        int x,y;
        cin>>x>>y;
        g.addEdge(x-1, y-1);
    }
    cout<< g.beautifulVertices() <<endl;
    return;
}

int main(){
    driverFunction();
    return 0;
}