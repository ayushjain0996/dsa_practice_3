#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * findAtK(node * head, int k){
    while(k--){
        head = head->next;
    }
    return head;
}
node* CreateLL(int n){
    if(n==0){
        return NULL;
    }
    node* head;
    int x;
    cin>>x;
    head = new node(x);
    node* tail = head;
    n--;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
void swap(node*& head, int i, int j){
    if(i==j){
        return;
    }
    if(i==0 && j==1){
        node * a = head;
        node * b = head->next;
        node * bnext = b->next;

        head = b;
        b->next = a;
        a->next = bnext;
        return;
    }
    if(j-i==1){
        node * aprev = findAtK(head, i-1);
        node * a = aprev->next;
        node * b = a->next;
        node * bnext = b->next;

        aprev->next = b;
        a->next = bnext;
        b->next = a;
        return;
    }
    
    node * aprev = findAtK(head, i-1);
    node * bprev = findAtK(head, j-1);
    node * a = aprev->next;
    node * b = bprev->next;
    node * bnext = b->next;

    aprev->next = b;
    b->next = a->next;
    bprev->next = a;
    a->next = bnext;
    return;
}
void bubbleSort(node *& head, int n){
    for(int i=0; i<n; i++){
        for(int j=0; j<n-i-1; j++){
            node * element = findAtK(head, j);
            if((element->data) > (element->next->data)){
                swap(head, j, j+1);
            }
        }
    }
    return;
}
void printLL(node* head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    cout<<endl;
    return;
}
int main(){
    int n;
    cin>>n;
    node * head = CreateLL(n);
    bubbleSort(head, n);
    printLL(head);
    return 0;
}