#include<iostream>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = NULL;
        right = NULL;
    }
};
node * CreateTree(){
    int x;
    cin>>x;
    node * root = new node(x);
    char str[6];
    cin>>str;
    if(str[0]=='t'){
        root->left = CreateTree();
    }
    cin>>str;
    if(str[0]=='t'){
        root->right = CreateTree();
    } 
    return root;   
}
bool isIdentical(node * root1, node * root2){
    if(root1==NULL && root2==NULL){
        return true;
    }
    else if(root1==NULL || root2==NULL){
        return false;
    }
    else{
        bool left = isIdentical(root1->left, root2->left);
        bool right = isIdentical(root1->right, root2->right);
        return left && right;
    }
}
int main(){
    node * root1 = CreateTree();
    node * root2 = CreateTree();
    if(isIdentical(root1, root2)){
        cout<<"true"<<endl;
    }
    else{
        cout<<"false"<<endl;
    }
    return 0;
}