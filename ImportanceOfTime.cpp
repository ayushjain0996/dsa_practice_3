#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * createLL(int n){
    if(n==0){
        return NULL;
    }
    n--;
    int x;
    cin>>x;
    node * head = new node(x);
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
void Rotate2Last(node *& head){
    node * temp = head;
    head = head->next;
    node * tail = temp;
    while(tail->next){
        tail = tail->next;
    }
    tail->next = temp;
    temp->next = NULL;
    return;
}
int timeTaken(node * head1, node * head2, int t){
    //Base Condition
    if(head1==NULL){
        return t;
    }
    //Recursive Call
    if(head1->data == head2->data){
        return timeTaken(head1->next, head2->next, t+1);
    }
    else{
        Rotate2Last(head1);
        return timeTaken(head1, head2, t+1);
    }
}
int main(){
    int n;
    cin>>n;
    node * head1 = createLL(n);
    node * head2 = createLL(n);
    cout<<timeTaken(head1, head2, 0)<<endl;
    return 0;
}