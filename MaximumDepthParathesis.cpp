#include<iostream>
#include<stack>

using namespace std;
int maxDepth(char str[]){
    int i=0;
    stack<char> s;
    int currDepth = 0;
    int MaxDepth = 0;
    while(str[i]!=0){
        if(str[i]=='('){
            currDepth++;
            s.push('(');
        }
        if(str[i]==')'){
            if(s.empty()){
                return -1;
            }
            currDepth--;
            s.pop();
        }
        if(currDepth > MaxDepth){
            MaxDepth = currDepth;
        }
        i++;
    }
    if(s.empty()){
        return MaxDepth;
    }
    return -1;
}

int main(){
    int t;
    cin>>t;
    char str[1000];
    cin.getline(str, 1000);
    while(t--){
        cin.getline(str, 1000);
        cout << maxDepth(str) <<endl;
    }    
    return 0;
}