// Code Problem ->  https://codeforces.com/problemset/problem/580/C

#include<iostream>
#include<vector>

using namespace std;

vector<vector<int> > adj;
vector<int> cats;
vector<bool> vis;

void dfs(int src, int count, int m){
    if(vis[src]){
        return;
    }
    if(count>m){
        return;
    }
    vis[src] = true;
    for(int i=0; i<adj[src].size(); i++){
        if(cats[i]==1){
            dfs(i, count+1, m);
        }
        else{
            dfs(i, 0, m);
        }
    }
    return;
}

int main(){
    int n, m;
    cin>>n>>m;
    vector<int> temp;
    vector<vector<int> > graph(n, temp);
    adj = graph;
    for(int i=0; i<n; i++){
        int x;
        cin>>x;
        cats.push_back(x);
        vis.push_back(false);
    }
    for(int i=0; i<n-1; i++){
        int u, v;
        cin>>u>>v;
        adj[u-1].push_back(v-1);
        adj[v-1].push_back(u-1);
    }
    dfs(0, cats[0], m);
    int ans = 0;
    for(int i=0; i<n; i++){
        if(adj[i].size()==1 && vis[i]==true){
            cout<< i+1 <<endl;
            ans++;
        }
    }
    cout<<"Total Restaurants: "<<ans<<endl;
    for(int i=0; i<n; i++){
        cout<<vis[i]<<" ";
    }
    return 0;
}