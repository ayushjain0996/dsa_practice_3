#include<iostream>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
node * BuildBST(int arr[], int n){
    if(n<=0){
        return NULL;
    }
    if(n%2){
        node * root = new node(arr[n/2]);
        root->left = BuildBST(arr, n/2);
        root->right = BuildBST(&arr[n/2+1], n/2);
        return root;
    }
    else{
        node * root = new node(arr[n/2-1]);
        root->left = BuildBST(arr, n/2-1);
        root->right = BuildBST(&arr[n/2], n/2);
        return root;
    }
}
void PreOrder(node * root){
    if(root==NULL){
        return;
    }
    cout<<root->data<<" ";
    PreOrder(root->left);
    PreOrder(root->right);
    return;
}
int main(){
    int n;
    int arr[1000];
    cin>>n;
    int sum = 0;
    for(int i=0; i<n; i++){
        cin>>arr[i];
        sum += arr[i];
    }
    for(int i=0; i<n; i++){
        int temp = arr[i];
        arr[i] = sum;
        sum = sum - temp;
    }
    node * root = BuildBST(arr, n);
    PreOrder(root);
    cout<<endl;
    return 0;
}