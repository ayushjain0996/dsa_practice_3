#include<iostream>
#include<map>
#include<unordered_map>

using namespace std;

int main(){
    int n;
    cin>>n;
    
    unordered_map<int, int> arr;
    for(int i=0; i<n; i++){
        int x;
        cin>>x;
        arr[x]++;
    }
    unordered_map<int, int> :: iterator it;
    int maxFreq = 0;
    int maxFreqVal = 0;
    for(it=arr.begin(); it!=arr.end(); it++){
        if(maxFreq < it->second){
            maxFreq = it->second;
            maxFreqVal = it->first;
        }
    }
    cout<<maxFreqVal<<endl;
    return 0;
}