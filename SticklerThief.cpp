//  https://practice.geeksforgeeks.org/problems/stickler-theif/0

#include <iostream>
#include<algorithm>

using namespace std;

int maxSum(int arr[], int i,int n){
    if(i>=n){
        return 0;
    }
    return max(arr[i]+ maxSum(arr, i+2, n), maxSum(arr, i+1, n));    
}

int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    cout<<maxSum(arr, 0, n)<<endl;
    return 0;
}