#include<iostream>
#include<set>

using namespace std;

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        set <int> s;
        for(int i=0; i<n; i++){  
            int x;
            cin>>x;
            s.insert(x);
        }

        int q;
        cin>>q;
        while(q--){
            int val;
            cin>>val;
            if(s.find(val)!=s.end()){
                cout<<"Yes"<<endl;
            }
            else{
                cout<<"No"<<endl;
            }
        }
    }
    return 0;
}