#include<iostream>
using namespace std;
struct node
{
    int data; 
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * CreateLL(){
    int x;
    cin>>x;
    if(x==-1){
        return NULL;
    }
    node * head = new node(x);
    node * tail = head;
    cin>>x;
    while(x!=-1){
        tail->next = new node(x);
        tail = tail->next;
        cin>>x;
    }
    return head;
}
node * KthElement(node * head, int k){
    node * pos1 = head;
    node * pos2 = head;
    while(k-- && pos2){
        pos2 = pos2->next;
    }
    while(pos2 && pos2){
        pos1 = pos1->next;
        pos2 = pos2->next;
    }
    return pos1;
}
int main(){
    node * head = CreateLL();
    int k;
    cin>>k;
    cout<< KthElement(head, k)->data <<endl;
    return 0;
}