#include<iostream>
#include<stack>
#include<vector>

using namespace std;
bool isPrime(int n){
    for(int i=2; i<n; i++){
        if(n%i==0){
            return false;
        }
    }
    return true;
}
int main(){
    int n;
    int q;
    cin>>n>>q;
    stack<int> A0, A1;
    while(n--){
        int x;
        cin>>x;
        A0.push(x);
    }
    vector< stack<int> > B;
    vector<int> prime;
    int num = 2;
    //Vector of Prime Numbers
    for(int i=0; i<q; i++){
        while(!isPrime(num)){
            num++;
        }    
        prime.push_back(num);
        num++;
    }
    stack<int> S;
    //Initialize Stack vector B
    for(int i=0; i<q; i++){
        B.push_back(S);
    }    
    for(int i=0; i<q; i++){
        if(A1.empty()){
            while(!A0.empty()){
                int x = A0.top();
                if((x%prime[i])==0){
                    B[i].push(x);
                }
                else{
                    A1.push(x);
                }
                A0.pop();
            }
        }
        else{
            while(!A1.empty()){
                int x = A1.top();
                if(x%prime[i]==0){
                    B[i].push(x);
                }
                else{
                    A0.push(x);
                }
                A1.pop();
            }
        }
    }
    for(int i=0; i<q; i++){
        while(!B[i].empty()){
            cout<< B[i].top() <<endl;
            B[i].pop();
        }
    }
    while(!A0.empty()){
            cout<< A0.top() <<endl;
            A0.pop();
        }
    while(!A1.empty()){
        cout<< A1.top() <<endl;
        A1.pop();
    }
    return 0;
}