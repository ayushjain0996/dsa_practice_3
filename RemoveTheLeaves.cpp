#include<iostream>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = NULL;
        right = NULL;
    }
};
node * CreateTree(){
    int x;
    cin>>x;
    node * root = new node(x);
    char str[6];
    cin>>str;
    if(str[0]=='t'){
        root->left = CreateTree();
    }
    cin>>str;
    if(str[0]=='t'){
        root->right = CreateTree();
    }
    return root;
}
node * RemoveLeaves(node * root){
    //Base Condition
    if(root==NULL){
        return NULL;
    }
    if(root->left==NULL && root->right==NULL){
        delete root;
        return NULL;
    }
    //Recursive Call
    root->left = RemoveLeaves(root->left);
    root->right = RemoveLeaves(root->right);
    return root;
}
void printTree(node * root){
    //Base Condition
    if(root==NULL){
        return;
    }
    //Recursive Call
    if(root->left==NULL){
        cout<<"END";
    }
    else{
        cout<< root->left->data;
    }
    cout<<" => "<< root->data <<" <= ";
    if(root->right==NULL){
        cout<<"END";
    }
    else{
        cout<< root->right->data;
    }
    cout<<endl;
    printTree(root->left);
    printTree(root->right);
    return;
}
int main(){
    node * root = CreateTree();
    root = RemoveLeaves(root);
    printTree(root);
    return 0;
}