#include<iostream>
#include<list>
#include<queue>


using namespace std;

class Graph{
    int V;
    list<int> *adjList;

    public:
    Graph(int v){
        V = v;
        adjList = new list<int>[V];
    }
    void addEdge(int u, int v, bool bidir = true){
        adjList[u].push_back(v);
        if(bidir){
            adjList[v].push_back(u);
        }
    }
    void printAdjList(){
        for(int i=0; i<V; i++){
            cout<<i<<"-> ";
            list<int> :: iterator it;
            for(it=adjList[i].begin(); it!=adjList[i].end(); it++){
                cout<< (*it) <<", ";
            }
            cout<<endl;
        }
    }

    void bfs(int src, int dest){
        queue<int> q;
        bool * visited = new bool[V]{0};
        int *distance = new int[V+1]{0};
        int *parent = new int[V+1];

        for(int i=0; i<=V; i++){
            parent[i] = -1;
        }

        q.push(src);
        visited[src] = true;

        while(!q.empty()){
            int node = q.front();
            cout<< node <<" ";
            q.pop();

            list<int> :: iterator it;
            for(it=adjList[node].begin(); it!=adjList[node].end(); it++){
                int neighbour = *it;
                if(!visited[neighbour]){
                    q.push(neighbour);
                    visited[neighbour] = true;
                    distance[neighbour] = distance[node] + 1;
                    parent[neighbour] = node;
                }
            }
        }
        cout<<endl;
        for(int i=0; i<V; i++){
            cout<<i<<" Node distance is "<<distance[i]<<endl;
        }

        cout<<"Shortest distance is "<<distance[dest]<<endl;
        cout<<"Shortest Path is: ";

        int temp = dest;
        while(temp!=-1){
            cout<<"<-"<<temp;
            temp = parent[temp];
        }
    }
};

int main(){
    Graph g(6);
    g.addEdge(0,1);
    g.addEdge(0,4);
    g.addEdge(1,2);
    g.addEdge(2,3);
    g.addEdge(2,4);
    g.addEdge(4,3);
    g.addEdge(3,5);

    // g.printAdjList();

    g.bfs(0, 5);

    return 0;
}