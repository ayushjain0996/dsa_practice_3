#include<iostream>
using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
node * CreateTree(int preorder[], int inorder[], int sizeIn, int sizePre){
    if(sizeIn<=0 || sizePre<=0){
        return NULL;
    }
    node * root = new node(preorder[0]);
    int i=0;
    while(inorder[i]!=preorder[0]){
        i++;
    }
    root->left = CreateTree(&preorder[1], inorder, i, i);
    root->right = CreateTree(&preorder[i+1], &inorder[i+1], sizeIn-i-1, sizePre-i-1);
    return root;
}
void PrintTree(node * root){
    if(root==NULL){
        return;
    }
    if(root->left==NULL){
        cout<<"END";
    }
    else{
        cout<< root->left->data;
    }
    cout<<" => "<< root->data <<" <= ";
    if(root->right==NULL){
        cout<<"END";
    }
    else{
        cout<<root->right->data;
    }
    cout<<endl;
    PrintTree(root->left);
    PrintTree(root->right);
    return;
}
int main(){
    int Inorder[10000], Preorder[10000];
    int n,m;
    cin>>n;
    for(int i=0; i<n; i++){
        cin>>Inorder[i];
    }
    cin>>m;
    for(int i=0; i<n; i++){
        cin>>Preorder[i];
    }
    node * root = CreateTree(Inorder, Preorder, n, m);
    PrintTree(root);
    return 0;
}