#include<iostream>
#include<queue>

using namespace std;

struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};

node * createTree(){
    int data;
    cin>>data;
    if(data==-1){
        return NULL;
    }
    node * root = new node(data);
    queue<node *> q1, q2;
    q1.push(root);
    while(!q1.empty() || !q2.empty()){
        while(!q1.empty()){
            node * top = q1.front();
            q1.pop();
            cin>>data;
            if(data!=-1){
                top->left = new node(data);
                q2.push(top->left);
            }
            cin>>data;
            if(data!=-1){
                top->right = new node(data);
                q2.push(top->right);
            }
        }
        while(!q2.empty()){
            node * top = q2.front();
            q2.pop();
            cin>>data;
            if(data!=-1){
                top->left = new node(data);
                q1.push(top->left);
            }
            cin>>data;
            if(data!=-1){
                top->right = new node(data);
                q1.push(top->right);
            }
        }
    }
    return root;
}

void rightView(node* root){
    if(root==NULL){
        return;
    }
    queue<node *> q1, q2;
    q1.push(root);
    while(!q1.empty() || !q2.empty()){
        if(!q1.empty()){
            node * top = q1.front();
            cout<< top->data <<" ";
        }
        while(!q1.empty()){
            node * top = q1.front();
            q1.pop();
            if(top->right){
                q2.push(top->right);
            }
            if(top->left){
                q2.push(top->left);
            }
        }
        if(!q2.empty()){
            node * top = q2.front();
            cout<< top->data <<" ";
        }
        while(!q2.empty()){
            node * top = q2.front();
            q2.pop();
            if(top->right){
                q1.push(top->right);
            }
            if(top->left){
                q1.push(top->left);
            }
        }
    }
    return;
}
int main(){
    node * root = createTree();
    rightView(root);
    cout<<endl;
    return 0;
}