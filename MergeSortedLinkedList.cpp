#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * CreateLL(int n){
    if(n==0){
        return NULL;
    }
    n--;
    int x;
    cin>>x;
    node * head = new node(x);
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
node * mergeLL(node * head1, node * head2){
    if(head1==NULL){
        return head2;
    }
    if(head2==NULL){
        return head1;
    }
    if(head1->data < head2->data){
        node * result = head1;
        node * chain = result;
        head1 = head1->next;
        while(head1 && head2){
            if(head1->data < head2->data){
                chain->next = head1;
                head1 = head1->next;
            }
            else{
                chain->next = head2;
                head2 = head2->next;
            }
            chain = chain->next;
        }
        if(head2==NULL){
            chain->next = head1;
        }
        if(head1==NULL){
            chain->next = head2;
        }
        return result;
    }
    else{
        node * result = head2;
        node * chain = result;
        head2 = head2->next;
        while(head1 && head2){
            if((head1->data) < (head2->data)){
                chain->next = head1;
                head1 = head1->next;
            }
            else{
                chain->next = head2;
                head2 = head2->next;
            }
            chain = chain->next;
        }
        if(head2==NULL){
            chain->next = head1;
        }
        if(head1==NULL){
            chain->next = head2;
        }
        return result;
    }
}
void printLL(node * head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    cout<<endl;
    return;
}
int main(){
    int T;
    cin>>T;
    while(T--){
        int n1;
        cin>>n1;
        node * head1 = CreateLL(n1);
        int n2;
        cin>>n2;
        node * head2 = CreateLL(n2);
        node * result = mergeLL(head1, head2);
        printLL(result);
    }
    return 0;
}