#include<iostream>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        left = right = NULL;
    }
};
node * CreateTree(){
    int x;
    cin>>x;
    node * root = new node(x);

    char str[6];
    cin>>str;
    if(str[0]=='t'){
        root->left = CreateTree();
    }
    cin>>str;
    if(str[0]=='t'){
        root->right = CreateTree();
    }
    return root;
}
pair<int , bool>isBalanced(node * root){
    //Base Condition
    if(root==NULL){
        pair<int, bool> p(0, true);
        return p;
    }
    pair<int, bool> left = isBalanced(root->left);
    pair<int, bool> right = isBalanced(root->right);
    if(left.second==true && right.second==true){
        if((left.first - right.first >= -1) && (left.first - right.first<=1)){
            pair<int, bool> p;
            if(left.first>right.first){
                p.first = left.first + 1;
            }
            else{
                p.first = right.first + 1;
            }
            p.second = true;
            return p;
        }
        else{
            pair<int, bool> p;
            if(left.first > right.first){
                p.first = left.first + 1;
            }
            else{
                p.first = right.first + 1;
            }
            p.second = false;
            return p;
        }
    }
    else{
        pair<int, bool> p;
        if(left.first > right.second){
            p.first = left.first + 1;
            p.second = false;
            return p;
        }
        else{
            p.first = right.first + 1;
            p.second = false;
            return p;
        }
    }
}
int main(){
    node * root = CreateTree();
    if(isBalanced(root).first){
        cout<<"true"<<endl;
    }
    else{
        cout<<"false"<<endl;
    }
    return 0;
}