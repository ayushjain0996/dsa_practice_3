#include<iostream>
#include<algorithm>

using namespace std;

bool compare(pair<int, int> a, pair<int, int> b){
    return(a.first<b.first);
}

int maxLength(pair<int, int> chain[], int n){
    sort(chain, chain+n, compare);

    int * arr = new int[n];
    for(int i=0; i<n; i++){
        arr[i] = 1;
    }
    for(int i=1; i<n; i++){
        for(int j=0; j<i; j++){
            if(chain[j].second < chain[i].first && arr[j]+1 > arr[i]){
                arr[i] = arr[j] + 1;
            }
        }
    }
    int maxVal = 0;
    for(int i=0; i<n; i++){
        if(maxVal < arr[i]){
            maxVal = arr[i];
        }
    }
    delete[] arr;
    return maxVal;
}

int main(){
    int t;
    cin>>t;
    while(t--){
        int n;
        cin>>n;
        pair<int, int> * arr = new pair<int, int>[n];
        for(int i=0; i<n; i++){
            cin>> arr[i].first  >> arr[i].second;
        }
        cout<< maxLength(arr, n)<<endl;
        delete[] arr;
    }
    return 0;
}