#include<iostream>
#include<stack>

using namespace std;

void Reverse(stack<int> &S){
    //Base Condition
    if(S.empty()){
        return;
    }
    //Recursive Call
    int x = S.top();
    S.pop();
    Reverse(S);
    S.push(x);
    return;
}
int main(){
    int n;
    cin>>n;
    stack<int> S;
    int x;
    for(int i=0; i<n; i++){
        cin>>x;
        S.push(x);
    }    
    Reverse(S);
    for(int i=0; i<n; i++){
        cout<<S.top()<<" ";
        S.pop();
    }
    return 0;
}