#include<iostream>
#include<algorithm>

using namespace std;

struct node{
    int data;
    int currMin;
    node * next;
    node (int data, int currMin){
        this->data = data;
        this->currMin = currMin;
        next = NULL;
    }
};

class Stack{
    node * head = NULL;

    public:
    void push(int data){
        if(head==NULL){
            head = new node(data, data);
            return;
        }
        else{
            int currMin = min(head->currMin, data);
            node * curr = new node(data, currMin);
            curr->next = head;
            head = curr;
            return;
        }
    }
    int getMin(){
        if(head==NULL){
            //error statement
            return -1;
        }
        else{
            return head->currMin;
        }
    }
    void pop(){
        if(head==NULL){
            return;
        }
        node * curr = head;
        head = head->next;
        delete curr;
        return;
    }
};

int main(){
    Stack s;
    int n;
    cin>>n;
    for(int i=0; i<n; i++){
        int x;
        cin>>x;
        s.push(x);
        cout<<"Min :"<<s.getMin()<<endl;
    }
    for(int i=0; i<n; i++){
        cout<< s.getMin() <<" ";
        s.pop();
    }
    return 0;
}