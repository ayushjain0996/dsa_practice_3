#include<iostream>
#include<vector>

using namespace std;
int min(int a,int b){
    if(a<b){
        return a;
    }
    return b;
}

int BC(int n, int k){
    vector<vector<long long int> > coef;
    for(int i=0; i<=n; i++){
        vector<long long int> v;
        coef.push_back(v);
    }
    for(int i=0; i<=n; i++){
        for(int j=0; j<=min(i,k); j++){
            if(j==0 || j==i){
                coef[i].push_back(1);
            }
            else{
                int curr = coef[i-1][j] + coef[i-1][j-1];
                coef[i].push_back(curr);
            }
        }
    }
    return coef[n][k];
}

int main(){
    int n,k;
    cin>>n>>k;
    cout<< BC(n,k) <<endl;

    return 0;
}