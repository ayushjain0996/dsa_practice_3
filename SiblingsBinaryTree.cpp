#include<iostream>

using namespace std;
struct node{
    int data;
    node * left;
    node * right;
    node(int data){
        this->data = data;
        this->left = NULL;
        this->right = NULL;
    }
};
node * CreateTree(){
    int x;
    cin>>x;
    node * root = new node(x);
    char str[6];
    cin>>str;
    if(str[0]=='t'){
        root->left = CreateTree();
    }
    cin>>str;
    if(str[0]=='t'){
        root->right = CreateTree();
    }
    return root;
}
void AntiSibling(node * root){
    //Base Condition
    if(root==NULL){
        return;
    }
    //Recursive Call
    if(root->left!=NULL && root->right==NULL){
        cout<< root->left->data <<" ";
    }
    AntiSibling(root->left);
    if(root->left==NULL && root->right!=NULL){
        cout<< root->right->data <<" ";
    }
    AntiSibling(root->right);
    return;
}
int main(){
    node * root = CreateTree();
    AntiSibling(root);
    cout<<endl;
    return 0;
}