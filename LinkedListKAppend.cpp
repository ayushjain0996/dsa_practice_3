#include<iostream>
using namespace std;
struct node{
    int data;
    node * next;
    node(int data){
        this->data = data;
        this->next = NULL;
    }
};
node * CreateLL(int n){
    if(n==0){
        return NULL;
    }
    int x;
    cin>>x;
    n--;
    node * head = new node(x);
    node * tail = head;
    while(n--){
        cin>>x;
        tail->next = new node(x);
        tail = tail->next;
    }
    return head;
}
void PrintLL(node * head){
    while(head){
        cout<< head->data <<" ";
        head = head->next;
    }
    cout<<endl;
    return;
}
void Append(node *& head){
    node * tail = head;
    while(tail->next->next){
        tail = tail->next;
    }
    tail->next->next = head;
    head = tail->next;
    tail->next = NULL;
}
int main(){
    int n;
    cin>>n;
    node * head = CreateLL(n);
    int k;
    cin>>k;
    while(k--){
        Append(head);
    }
    PrintLL(head);
    return 0;
}