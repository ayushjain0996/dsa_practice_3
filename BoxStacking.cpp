#include<iostream>
#include<algorithm>

using namespace std;

struct box{
    int w;
    int b;
    int h;
};

bool compare(box a, box b){
    return (a.b*a.w > b.b*b.w);
}

int maxHeight(box arr[], int n){
    box * rot = new box[3*n];
    int index = 0;
    for(int i=0; i<n; i++){
        rot[index] = arr[i];
        index++;

        rot[index].b = arr[i].h;
        rot[index].h = arr[i].w;
        rot[index].w = arr[i].b;
        index++;

        rot[index].b = arr[i].w;
        rot[index].h = arr[i].b;
        rot[index].w = arr[i].h;
        index++;
    }
    n = 3*n;
    sort(rot, rot+n, compare);

    int * msh = new int[n];
    for(int i=0; i<n; i++){
        msh[i] = rot[i].h;
    }

    for(int i=1; i<n; i++){
        for(int j=0; j<i; j++){
            if(rot[i].b<rot[j].b && rot[i].w<rot[j].w){
                if(msh[i]<msh[j] + rot[j].h){
                    msh[i] = msh[j] + rot[j].h;
                }
            }
        }
    }
    int maxHeight = -1;
    for(int i=0; i<n; i++){
        if(maxHeight<msh[i]){
            maxHeight = msh[i];
        }
    }
    
    int result = maxHeight;
    delete[] msh;
    delete[] rot;
    return result;
}

int main(){
    int n;
    cin>>n;
    box * arr = new box[n];
    for(int i=0; i<n; i++){
        int x,y,z;
        cin>>x>>y>>z;
        arr[i].b = x;
        arr[i].h = y;
        arr[i].w = z;
    }
    cout<<maxHeight(arr, n)<<endl;
    delete[] arr;
    return 0;
}