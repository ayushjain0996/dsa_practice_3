#include<iostream>
#include<stack>

using namespace std;
bool isBalanced(char str[], int i, stack<char> &S){
    //Base Condition
    if(str[i]=='\0'){
        if(S.empty())
            return true;
        else
        {
            return false;
        }
        
    }
    //Recusrive Call
    if(str[i]=='(' || str[i]=='[' || str[i]=='{'){
        S.push(str[i]);
    }
    else if(str[i]==')' || str[i]=='}' || str[i]==']'){
        if(S.empty()){
            return false;
        }
        if(str[i]==')'){
            char top = S.top();
            if(top!='('){
                return false;
            }
            else{
                S.pop();
            }
        }
        else if(str[i]=='}'){
            char top = S.top();
            if(top!='{'){
                return false;
            }
            else{
                S.pop();
            }
        }
        else{
            char top = S.top();
            if(top!='['){
                return false;
            }
            else{
                S.pop();
            }
        }
    }
    
    return isBalanced(str, i+1, S);
}

int main(){
    char str[1000];
    cin>>str;
    stack<char> S;
    if(isBalanced(str, 0, S)){
        cout<<"true";
    }
    else{
        cout<<"false";
    }
    return 0;
}