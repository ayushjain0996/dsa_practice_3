#include<iostream>
#include<list>
#include<queue>

using namespace std;

class Graph{
    int V;
    list<int> *adjList;

    public:
    Graph(int v){
        V = v;
        adjList = new list<int>[V];
    }

    void addEdge(int u, int v, bool bidir = true){
        adjList[u-1].push_back(v-1);
        if(bidir){
            adjList[v-1].push_back(u-1);
        }
    }

    void bfs(int src){
        queue<int> q;
        bool * visited = new bool[V+1]{0};
        int * distance = new int[V+1];

        for(int i=0; i<V; i++){
            distance[i] = -1;
        }
        
        q.push(src-1);
        visited[src-1] = true;
        distance[src-1] = 0;

        while(!q.empty()){
            int currNode = q.front();
            q.pop();

            list<int> :: iterator it;
            for(it=adjList[currNode].begin(); it!=adjList[currNode].end(); it++){
                int neighbour = *it;
                if(!visited[neighbour]){
                    q.push(neighbour);
                    visited[neighbour] = true;
                    distance[neighbour] = distance[currNode] + 6;
                }
            }
        }
        for(int i=0; i<V; i++){
            if(i!=src-1){
                cout<<distance[i]<<" ";
            }
        }
        cout<<endl;
    }
};

int main(){
    int t;
    cin>>t;
    while(t--){
        int N, M;
        cin>>N>>M;
        Graph g(N);
        while(M--){
            int x, y;
            cin>>x>>y;
            g.addEdge(x,y);
        }
        int src;
        cin>>src;
        g.bfs(src);
    }
    return 0;
}